@extends('layouts.home-app')
@section('title')
    Detail Hasil Seleksi Lowongan
@endsection
@section('content')



    <body style="background: #f6f6f6">
        <div class="container-fluid mt-5">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-8 col-sm-12 mt-5" style="background: white;padding-bottom: 15px;">
                    <img src="{{ asset('home/images/bps-logo.png') }}" width="100%">
                    <h2 class="text-center mt-3 mb-3">PENGUMUMAN HASIL SELEKSI LOWONGAN</h2>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <div class="box-img"
                                style="background-repeat: no-repeat;background-position: center center;background-size: cover;padding-bottom: 120%;min-width: 100%;background-image: url( {{ asset('uploads/avatar/partner/' . $gambar) }} );">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <table style="width: 100%;height:100%;background: #f6f6f6;padding: 5px 10px">
                                <tbody>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">ID Peserta</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $id }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Nama Lengkap</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $nama }}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 25%;padding: 5px 10px">Tanggal Lahir</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $tgl }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Alamat</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $alamat }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">No HP</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $hp }}</td>
                                    </tr>
                                    <tr>
                                        @if ($status == 'lulus')
                                            <td colspan="3" style="padding-top: 12px;padding-left: 5px;font-weight: bolder">
                                                Selamat! Anda dinyatakan
                                                <span style="background: green;color: white;padding: 5px 10px">Lulus
                                                    Seleksi</span>
                                            </td>
                                        @else
                                            <td colspan="3" style="padding-top: 12px;padding-left: 5px;font-weight: bolder">
                                                Maaf, Anda dinyatakan
                                                <span style="background: red;color: white;padding: 5px 10px">Tidak Lulus
                                                    Seleksi</span>
                                            </td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Lowongan</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $lowongan }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Posisi</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $posisi }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Nilai</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $nilai }}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Keterangan</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $keterangan }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
