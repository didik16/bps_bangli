@extends('layouts.home-app')
@section('title')
    Tentang Kami
@endsection
@section('content')


    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-12">
                    <span class="caption">Lowongan</span>
                    <h2 class="title-with-line text-center mb-5">{{ $data->title }}</h2>
                    <p>
                        Berakhir Pada <span class="badge badge-primary">
                            {{ date('d F Y', strtotime($data->end_period)) }}
                        </span><br>
                        @foreach ($data->detail as $detail)
                            Posisi {{ $detail->position->name }}
                            <span class="badge badge-success text-white">
                                {{ $detail->current_partner }}/{{ $detail->max_partner }}
                            </span>
                        @endforeach
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{ asset('/uploads/vacancy/' . $data->photo) }}" alt="Image" class="img-fluid" width="250">
                </div>
                <div class="col-md-12 mt-5 mb-5">
                    {!! $data->description !!}
                </div>
                <div class="col-md-12 mb-5">
                    @if (\Auth::guard('partners')->check())
                        @if (\App\Partner::where('id', \Auth::guard('partners')->user()->id)
            ->with('detail')
            ->first()->detail->completeness == 100)
                            @if (!$vacancy_apply)
                                <form action="{{ route('home.vacancy.apply', $data->id) }}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Pilih Posisi Sebagai</label>
                                            <select class="form-control" name="position_id" required>
                                                <option value="">-- Pilih --</option>
                                                @foreach ($data->detail as $detail)
                                                    <option value="{{ $detail->position_id }}">{{ $detail->position->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for=""></label>
                                            <button class="btn btn-block btn-primary">Lamar Lowongan</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                @if ($vacancy_apply->status == 'UNAPPROVED')
                                    <div class="alert alert-primary text-center">
                                        Lamaran anda sedang dalam proses verifikasi oleh petugas.
                                    </div>
                                @elseif($vacancy_apply->status == "APPROVED")
                                    <div class="alert alert-success text-center">
                                        Selamat! Lamaran anda sudah diterima oleh petugas.
                                    </div>

                                @elseif($vacancy_apply->status == "INTERVIEW")
                                    <div class="alert alert-info text-center">
                                        Selamat!, anda lolos tahap administrasi, selanjutnya tahap Interview.
                                    </div>
                                @else
                                    <div class="alert alert-danger text-center">
                                        Maaf, lamaran anda ditolak oleh petugas. Silahkan anda mengulangi proses pendaftaran
                                        lowongan.
                                    </div>

                                    <form action="{{ route('home.vacancy.apply', $data->id) }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Pilih Posisi Sebagai</label>
                                                <select class="form-control" name="position_id" required>
                                                    <option value="">-- Pilih --</option>
                                                    @foreach ($data->detail as $detail)
                                                        <option value="{{ $detail->position_id }}">
                                                            {{ $detail->position->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for=""></label>
                                                <button class="btn btn-block btn-primary">Lamar Lowongan</button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            @endif
                        @else
                            <div class="alert alert-warning text-center">
                                Silahkan lengkapi profile anda terlebih dahulu untuk melakukan pelamaran lowongan ini.
                            </div>
                        @endif
                    @else
                        <div class="alert alert-danger text-center">
                            Silahkan login terlebih dahulu untuk melakukan pelamaran lowongan ini.
                        </div>
                    @endif
                </div>
                {{-- <div class="col-md-12 mb-5">
                    <button class="btn btn-primary btn-block">Lamar Lowongan</button>
                </div> --}}
            </div>
        </div>
    </div>

    @if (!Auth::guard('partners')->check() || Auth::guard('administrators')->check())

        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection
