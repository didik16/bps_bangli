@extends('admin.layouts.app')

@section('title')
    Edit Lowongan - {{ env("APP_NAME") }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Edit Lowongan</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.vacancy.index') }}">Lowongan</a></li>
                        <li class="breadcrumb-item"><a href="#!">Edit Data</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Form Lowongan</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('admin.vacancy.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Judul <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="title" value="{{ $data->title }}" aria-describedby="emailHelp" placeholder="Enter Name" >
                                    @error('title')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                          
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Deksripsi <span class="text-danger">*</span></label>
                                    <textarea name="description" id="ckeditor">{{ $data->description }}</textarea>
                                    @error('description')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="position">Posisi <span class="text-danger">*</span></label>
                                    <select name="" class="select2" name="positions[]" multiple="multiple" required>
                                        @foreach($positions as $position)
                                            <option value="{{ $position->id }}" data-name="{{ $position->name }}">{{ $position->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="max-partner-group">
                                    @foreach($data->detail as $key => $detail)
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Maksimal Partner untuk <b>{{ $detail->position->name }}</b><span class="text-danger">*</span></label>
                                            <input type="hidden" name="positions[{{$key}}][id]" value="{{ $detail->position_id}}"/>
                                            <input type="number" class="form-control" id="exampleInputPassword1" name="positions[{{ $key }}][max_partner]" value="{{ $detail->max_partner }}" min="1" placeholder="Max Partner" >
                                            @error('max_partner')
                                                <small class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Periode Berakhir <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" value="{{ date("Y-m-d", strtotime($data->end_period)) }}" id="exampleInputPassword1" name="end_period" placeholder="End Period" >
                                    @error('password')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status Lowongan</label>
                                    
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" value="1" class="custom-control-input" id="is-active" @if($data->status == "ACTIVE") {{ 'checked' }} @endif>
                                        <label class="custom-control-label" for="is-active">Aktif</label>
                                        @error('status')
                                            <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Foto Thumbnail <span class="text-danger">*</span></label>
                                    <input type="file" name="photo" class="dropify" data-default-file="{{ asset('/uploads/vacancy/' . $data->photo) }}"/>
                                    @error('photo')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <a href="{{ route('admin.vacancy.index') }}">
                                    <button type="button" class="btn  btn-secondary">Kembali</button>
                                </a>
                                <button type="submit" class="btn  btn-primary">Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    @push('scripts')
        <script>
            $(document).ready(function(){
                
                var selectedCategoryIds = [];

                @foreach($data->detail as $detail)
                    var categoryIds = (@php echo $detail @endphp);
                    selectedCategoryIds.push(categoryIds.position_id.toString());

                    console.log(selectedCategoryIds);
                @endforeach

                if(selectedCategoryIds.length > 0){
                    $('.select2').select2().val(selectedCategoryIds).trigger('change');
                }

                $('.select2').select2({
                    width: '100%'
                }).on('change', function(){
                    let data = ``;
                    let options = [];
                    $("#max-partner-group").html('');

                    var selected = $(this).find('option:selected', this);
                    selected.each(function(){
                        options.push({
                            id: $(this).val(),
                            name: $(this).attr('data-name')
                        })
                    })

                    $.each(options, function(key, val){

                        data += `
                            <div class="form-group">
                                    <label for="exampleInputPassword1">Max Partner for <b>${val.name}</b><span class="text-danger">*</span></label>
                                    <input type="hidden" name="positions[${key}][id]" value="${val.id}"/>
                                    <input type="number" class="form-control" id="exampleInputPassword1" name="positions[${key}][max_partner]" value="1" min="1" placeholder="Max Partner" >
                                    @error('max_partner')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                        `
                    });
                    $("#max-partner-group").html(data)
                })
            })
        </script>
    @endpush
@endsection