@extends('admin.layouts.app')

@section('title')
    Data Lowongan - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Lowongan</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Lowongan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Lowongan</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.vacancy.create') }}"><span><i
                                                class="feather icon-plus"></i> Tambah Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                @php $no=1;@endphp
                <div class="card-body">
                    <table id="datatable" class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Akhir Periode</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $vacancy)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    {{-- <td><img
                                            src="{{ $vacancy->photo ?? 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif' }}"
                                            alt="{{ $vacancy->name }}" width="100"></td> --}}

                                    @isset($vacancy->photo)
                                        <td><img src="{{ asset('/uploads/vacancy/' . $vacancy->photo) }}"
                                                alt=" {{ $vacancy->name }}" width="100"></td>
                                    @else
                                        <td><img src="https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif"
                                                alt="{{ $vacancy->name }}" width="100"></td>
                                    @endisset

                                    <td>{{ $vacancy->title }}</td>
                                    <td>{{ date('d F Y', strtotime($vacancy->end_period)) }}</td>
                                    <td>
                                        @if ($vacancy->status == 'ACTIVE')
                                            <span class="badge badge-primary">Aktif</span>
                                        @elseif($vacancy->status == "INACTIVE")
                                            <span class="badge badge-secondary">Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.vacancy.applies.index', $vacancy->id) }}">
                                            <button class="btn btn-success">Lihat Pelamar</button>
                                        </a>
                                        <a href="{{ route('admin.vacancy.edit', $vacancy->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
