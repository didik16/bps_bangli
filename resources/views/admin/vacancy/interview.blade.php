@extends('admin.layouts.app')

@section('title')
    Interview - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Tambah Interview</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>

                        <li class="breadcrumb-item"><a href="{{ route('admin.activity.index') }}">Interview</a></li>
                        <li class="breadcrumb-item"><a href="#!"> Interview</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Form Interview</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="/admin/store_interview" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{ $data->id }}" name="applies_id">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama <span class="text-danger"></span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                                        value="{{ $partner->name }}" aria-describedby="emailHelp" readonly>
                                    @error('description')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Lowongan</label>
                                        <input type="lowongan" class="form-control" id="inputEmail4"
                                            value="{{ $vacancy->title }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Posisi</label>
                                        <input type="posisi" class="form-control" id="inputPassword4"
                                            value="{{ $position->name }}" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tanggal <span class="text-danger">*</span></label>
                                    <input type="datetime-local" class="form-control date"
                                        value="{{ date('Y-m-d', strtotime('+1 day')) }}" id="exampleInputPassword1"
                                        name="tanggal" placeholder="End Period">
                                    @error('password')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pewawancara <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="pewawancara"
                                        value="{{ old('pewawancara') }}" aria-describedby="emailHelp"
                                        placeholder="Masukan Nama Pewawancara">
                                    @error('pewawancara')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <a href="{{ route('admin.activity.index') }}">
                                    <button type="button" class="btn  btn-secondary">Kembali</button>
                                </a>
                                <button type="submit" class="btn  btn-primary">Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>



@endsection
