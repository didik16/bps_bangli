@extends('admin.layouts.app')

@section('title')
    Interview - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Kirim Interview</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>

                        <li class="breadcrumb-item"><a href="{{ route('admin.activity.index') }}">Interview</a></li>
                        <li class="breadcrumb-item"><a href="#!">Kirim Interview</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Form Interview</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="/admin/store_submit_interview" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{ $data->id }}" name="applies_id">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama <span class="text-danger"></span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                                        value="{{ $partner->name }}" aria-describedby="emailHelp" readonly>
                                    @error('description')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Lowongan</label>
                                        <input type="lowongan" class="form-control" id="inputEmail4"
                                            value="{{ $vacancy->title }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Posisi</label>
                                        <input type="posisi" class="form-control" id="inputPassword4"
                                            value="{{ $position->name }}" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tanggal <span class="text-danger">*</span></label>
                                    <input type="datetime-local" class="form-control date"
                                        value="{{ date('Y-m-d\TH:i', strtotime($interview->tanggal)) }}"
                                        id="exampleInputPassword1" name="tanggal" placeholder="End Period" readonly>
                                    @error('password')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pewawancara <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="pewawancara"
                                        value="{{ $interview->pewawancara }}" aria-describedby="emailHelp">
                                    @error('pewawancara')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="nilai" name="nilai"
                                        value="{{ old('nilai') }}" required>
                                    @error('nilai')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Status <span class="text-danger">*</span></label>
                                    {{-- <select class="form-control" name="status" required>
                                        <option>Pilih status</option>
                                        <option value="APPROVED">Diterima</option>
                                        <option value="REJECTED">Ditolak</option>
                                    </select> --}}

                                    <input type="text" class="form-control" id="status" name="status"
                                        value="{{ old('status') }}" required readonly>
                                    @error('status')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Keterangan <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="keterangan"
                                        value="{{ old('keterangan') }}" required>
                                    @error('keterangan')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <a href="{{ route('admin.activity.index') }}">
                                    <button type="button" class="btn  btn-secondary">Kembali</button>
                                </a>
                                <button type="submit" class="btn  btn-primary">Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>

@section('script')
    <script type="text/javascript">
        window.onload = function() {
            $('#nilai').on('keyup', function() {
                var nilai = $('#nilai').val();

                if (nilai >= 0 && nilai <= 69) {
                    $("#status").val('Tidak Lulus');
                } else if (nilai >= 70 && nilai <= 100) {
                    $("#status").val('Lulus');
                }

            });

        }

    </script>

@endsection

@endsection
