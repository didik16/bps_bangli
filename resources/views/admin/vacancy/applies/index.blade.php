@extends('admin.layouts.app')

@section('title')
    Data Pelamar dari {{ $data->title }} - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Pelamar</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.vacancy.index') }}">Lowongan</a></li>
                        <li class="breadcrumb-item"><a href="#!">{{ $data->title }}</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h5>Info Kuota Pelamar</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Judul</b></label>
                        <p>{{ $data->title }}</p>
                    </div>
                    @foreach ($data->detail as $detail)
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Kuota dari {{ $detail->position->name }}</b></label>
                            <p>{{ $detail->current_partner }} / {{ $detail->max_partner }}</p>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Pelamar</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.vacancy.create') }}"><span><i
                                                class="feather icon-plus"></i> Tambah Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Nama Partner</th>
                                <th>Posisi</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data->applies as $apply)
                                <tr>
                                    <td><a
                                            href="{{ route('admin.partner.show', $apply->partner_id) }}">{{ $apply->partner->name }}</a>
                                    </td>
                                    <td>
                                        <span class="badge badge-primary">{{ $apply->position->name }}</span>
                                    </td>
                                    <td>
                                        @if ($apply->status == 'UNAPPROVED')
                                            <span class="badge badge-warning">Tidak Disetuji</span>
                                        @elseif($apply->status == "APPROVED")
                                            <span class="badge badge-success">Disetuji</span>
                                        @elseif($apply->status == "INTERVIEW")
                                            <span class="badge badge-info">Interview</span>
                                        @else
                                            <span class="badge badge-danger">Ditolak</span>
                                        @endif
                                    </td>
                                    <td>{{ date('d F Y', strtotime($apply->created_at)) }}</td>

                                    <td>
                                        @if ($apply->status == 'INTERVIEW')
                                            <a href="{{ '/admin/vacancy/' . $apply->id . '/submit_interview' }}">
                                                <button class="btn btn-success">Kirim Interview</button>
                                            </a>
                                            {{-- <a
                                                href="{{ route('admin.vacancy.applies.action', [$data->id, $apply->id]) }}?action=rejected">
                                                <button class="btn btn-danger">Reject</button>
                                            </a> --}}
                                        @endif


                                        @if ($apply->status == 'APPROVED')
                                            <button class="btn btn-success" disabled>Disetuji</button>
                                        @endif

                                        @if ($apply->status == 'REJECTED')
                                            <button class="btn btn-danger" disabled>Ditolak</button>
                                        @endif

                                        @if ($apply->status == 'UNAPPROVED')
                                            <a href="{{ '/admin/vacancy/' . $apply->id . '/interview' }}">
                                                <button class="btn btn-success">Setujui Administrasi</button>
                                            </a>
                                            <a
                                                href="{{ route('admin.vacancy.applies.action', [$data->id, $apply->id]) }}?action=rejected">
                                                <button class="btn btn-danger">Ditolak</button>
                                            </a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
