@extends('admin.layouts.app')

@section('title')
    Dashboard - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Dashboard Analisis</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard Analisis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- page statustic card start -->
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-yellow">{{ $announcement_count }}</h4>
                                    <h6 class="text-muted m-b-0">Pengumunan</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-bell f-28 text-c-yellow"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-yellow">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <p class="text-white m-b-0"></p>
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-green">{{ $activity_count }}</h4>
                                    <h6 class="text-muted m-b-0">Aktivitas</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-image f-28 text-c-green"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-green">
                            <div class="row align-items-center">
                                <div class="col-9">
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-purple">{{ $administrator_count }}</h4>
                                    <h6 class="text-muted m-b-0">Administrator</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-user f-28 text-c-purple"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-purple">
                            <div class="row align-items-center">
                                <div class="col-9">
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-red">{{ $position_count }}</h4>
                                    <h6 class="text-muted m-b-0">Posisi</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-user-check f-28 text-c-red"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-red">
                            <div class="row align-items-center text-c-red">
                                <div class="col-9">
                                    <p class="text-white m-b-0"></p>
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-blue">{{ $vacancy_count }}</h4>
                                    <h6 class="text-muted m-b-0">Lowongan</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-file-text f-28 text-c-blue"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-blue">
                            <div class="row align-items-center">
                                <div class="col-9">
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-yellow">{{ $partner_count }}</h4>
                                    <h6 class="text-muted m-b-0">Partner</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-users f-28 text-c-yellow"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-yellow">
                            <div class="row align-items-center">
                                <div class="col-9">
                                </div>
                                <div class="col-3 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page statustic card end -->
        </div>
        <!-- prject ,team member start -->
    </div>
@endsection
