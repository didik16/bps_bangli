@extends('admin.layouts.app')

@section('title')
    Data Pengumuman - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Pengumuman</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="#!">Pengumuman</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Pengumuman</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.announcement.create') }}"><span><i
                                                class="feather icon-plus"></i> Add Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                @php $no=1;@endphp
                <div class="card-body">
                    <table id="datatable" class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $announcement)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    {{-- <td><img
                                            src="{{ $announcement->photo ?? 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif' }}"
                                            alt="{{ $announcement->name }}" width="100"></td>
                                    --}}

                                    @isset($announcement->photo)
                                        <td><img src="{{ asset('/uploads/announcement/' . $announcement->photo) }}"
                                                alt=" {{ $announcement->name }}" width="100"></td>
                                    @else
                                        <td><img src="https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif"
                                                alt="{{ $announcement->name }}" width="100"></td>
                                    @endisset


                                    <td>
                                        <p style="width: 200px;">{{ $announcement->title }}</p>
                                    </td>
                                    <td>{!! Str::limit($announcement->description, 500) !!}</td>
                                    <td>
                                        @if ($announcement->status == 'PUBLISHED')
                                            <span class="badge badge-primary">Published</span>
                                        @elseif($announcement->status == "UNPUBLISHED")
                                            <span class="badge badge-secondary">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.announcement.edit', $announcement->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
