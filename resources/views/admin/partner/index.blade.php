@extends('admin.layouts.app')

@section('title')
    Data Mitra - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Mitra</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="#!">Mitra</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Mitra</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.partner.create') }}"><span><i
                                                class="feather icon-plus"></i> Tambah Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                @php $no=1;@endphp
                <div class="card-body">
                    <table id="datatable" class="table table-stripe table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Foto</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Profil</th>
                                <th>Bergabung</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $partner)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    @isset($partner->photo)
                                        <td><img src="{{ asset('uploads/avatar/partner/' . $partner->photo) }}"
                                                alt="{{ $partner->name }}" width="100"></td>
                                    @else
                                        <td><img src="{{ 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif' }}"
                                                alt="{{ $partner->name }}" width="100"></td>
                                    @endisset
                                    <td style="width: 5%">{{ $partner->name }}</td>
                                    <td>{{ $partner->email }}</td>
                                    <td>
                                        @if ($partner->status == 'ACTIVE')
                                            <span class="badge badge-primary">Aktif</span>
                                        @elseif($partner->status == "INACTIVE")
                                            <span class="badge badge-secondary">Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td><span class="badge badge-success">{{ $partner->detail->completeness }}%</span></td>
                                    <td>
                                        <p
                                            style="display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;
                                                                                                                        overflow: hidden;
                                                                                                                        text-overflow: ellipsis;
                                                                                                                        white-space: nowrap; ">
                                            {{ date('d F Y - H:i:s', strtotime($partner->created_at)) }}
                                        </p>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.partner.show', $partner->id) }}">
                                            <button class="btn btn-success">Detail</button>
                                        </a>
                                        <a href="{{ route('admin.partner.edit', $partner->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
