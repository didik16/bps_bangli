@extends('admin.layouts.app')

@section('title')
    Detail Partner - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Show Partner</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.partner.index') }}">Partners</a></li>
                        <li class="breadcrumb-item"><a href="#!">Detail Data</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    Profile Completion
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="progress mb-4">
                                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                    role="progressbar" aria-valuenow="{{ $data->detail->completeness }}" aria-valuemin="0"
                                    aria-valuemax="{{ $data->detail->completeness }}"
                                    style="width: {{ $data->detail->completeness }}%">{{ $data->detail->completeness }}%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <h5>Photo Partner</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{ asset('uploads/avatar/partner/' . $data->photo) }}" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h5>Info Partner</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Name</b></label>
                                    <p>{{ $data->name }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Email</b></label>
                                    <p>{{ $data->email }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Address</b></label>
                                    <p>{{ $data->detail->address }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Birth</b></label>
                                    <p>
                                        @if ($data->detail->birth_place && $data->detail->birth_date)
                                            {{ $data->detail->birth_place }},
                                            {{ date('d F Y', strtotime($data->detail->birth_date)) }}
                                        @else
                                            -
                                        @endif
                                    </p>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Is Married</b></label>
                                    <p>
                                        @if ($data->detail->is_marriage)
                                            {{ $data->detail->is_marriage == '1' ? 'Yes' : 'No' }}
                                        @else
                                            -
                                        @endif
                                    </p>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Phone Number</b></label>
                                    <p>{{ $data->detail->phone_number ?? '-' }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Phone Type</b></label>
                                    <p>{{ $data->detail->phone_type ?? '-' }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Last Education</b></label>
                                    <p>{{ $data->detail->last_education ?? '-' }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Last Activity</b></label>
                                    <p>{{ $data->detail->last_activity ?? '-' }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Is Active</b></label>
                                    <p>
                                        @if ($data->status == 'ACTIVE')
                                            <span class="badge badge-primary">Active</span>
                                        @elseif($data->status == "INACTIVE")
                                            <span class="badge badge-secondary">Inactive</span>
                                        @endif
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><b>Joined At</b></label>
                                    <p>{{ date('d F Y - H:i:s', strtotime($data->created_at)) }}</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Detail File</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table table-striped text-center">
                            <thead class="thead-light">
                                <tr>
                                    <th>KTP</th>
                                    <th>KK</th>
                                    <th>Ijazah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @if ($data->detail->ktp)
                                            <img src="{{ asset('uploads/partner/files/' . $data->detail->ktp) }}" alt=""
                                                width="200px">
                                        @else
                                            -
                                        @endif

                                    </td>
                                    <td>
                                        @if ($data->detail->kk)
                                            <img src="{{ asset('uploads/partner/files/' . $data->detail->kk) }}" alt=""
                                                width="200px">
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if ($data->detail->ijazah)
                                            <img src="{{ asset('uploads/partner/files/' . $data->detail->ijazah) }}" alt=""
                                                width="200px">
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
@endsection
