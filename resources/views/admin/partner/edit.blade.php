@extends('admin.layouts.app')

@section('title')
    Edit Partner - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Edit Partner</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.partner.index') }}">Partners</a></li>
                        <li class="breadcrumb-item"><a href="#!">Edit Data</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h5>Form Partner</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('admin.partner.update', $data->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                                        value="{{ $data->name }}" aria-describedby="emailHelp" placeholder="Enter Name">
                                    @error('name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                                        value="{{ $data->email }}" aria-describedby="emailHelp" placeholder="Enter email">
                                    @error('email')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" name="password"
                                        placeholder="Password">
                                    @error('password')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password Confirmation <span
                                            class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="exampleInputPassword1"
                                        name="password_confirmation" placeholder="Password">
                                    @error('password')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status Account</label>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" value="1" class="custom-control-input"
                                            id="is-active" @if ($data->status == 'ACTIVE')
                                        {{ 'checked' }} @endif>
                                        <label class="custom-control-label" for="is-active">Active</label>
                                        @error('status')
                                            <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Photo Profile</label>
                                    <input type="file" name="photo" class="dropify"
                                        data-default-file="{{ asset('/uploads/avatar/partner/' . $data->photo) }}" />
                                    @error('photo')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <a href="{{ route('admin.partner.index') }}">
                                    <button type="button" class="btn  btn-secondary">Back</button>
                                </a>
                                <button type="submit" class="btn  btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
@endsection
