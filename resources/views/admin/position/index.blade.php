@extends('admin.layouts.app')

@section('title')
    Data Posisi - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Posisi</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Posisi</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Posisi</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.position.create') }}"><span><i
                                                class="feather icon-plus"></i> Tambah Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                @php $no=1;@endphp
                <div class="card-body">
                    <table id="datatable" class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $position)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $position->name }}</td>
                                    <td>{!! $position->deskripsi !!}</td>
                                    <td>
                                        @if ($position->status == 'ACTIVE')
                                            <span class="badge badge-primary">Aktif</span>
                                        @elseif($position->status == "INACTIVE")
                                            <span class="badge badge-secondary">Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.detail_position', $position->id) }}">
                                            <button class="btn btn btn-success">Lihat Riwayat Mitra</button>
                                        </a>
                                        <a href="{{ route('admin.position.edit', $position->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
