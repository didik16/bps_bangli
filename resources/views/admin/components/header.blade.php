<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar menu-light ">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div ">

            <div class="">
                <div class="main-menu-header">
                    <img class="img-radius" style="width: 60px; height: 60px"
                        src="{{ \Auth::guard('administrators')->user()->photo ?? 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif' }}"
                        alt="User-Profile-Image">
                    <div class="user-details">
                        <div id="more-details">{{ \Auth::guard('administrators')->user()->name }} <i
                                class="fa fa-caret-down"></i></div>
                    </div>
                </div>
                <div class="collapse" id="nav-user-link">
                    <ul class="list-unstyled">
                        <li class="list-group-item"><a
                                href="{{ route('admin.administrator.edit', \Auth::guard('administrators')->user()->id) }}"><i
                                    class="feather icon-user m-r-5"></i>Edit Profile</a></li>
                        <li class="list-group-item"><a href="{{ route('admin.logout') }}"><i
                                    class="feather icon-log-out m-r-5"></i>Logout</a></li>
                    </ul>
                </div>
            </div>

            @include('admin.components.sidebar')

        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->
<!-- [ Header ] start -->
<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">


    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="{{ route('home.index') }}" class="b-brand">
            <!-- ========   change your logo hear   ============ -->
            <img src="https://banglikab.bps.go.id/backend/images/Header-Frontend-Besar-ind.png" alt="" class="logo"
                style="width: 150px">
            <img src="https://banglikab.bps.go.id/backend/images/Header-Frontend-Besar-ind.png" alt=""
                class="logo-thumb">
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown dropdown-notifications">
                    <a href="" id="dropdownMenuButton" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="far fa-bell notification-icon" data-count="0"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right notification">
                        <div class="noti-head ">
                            <h6 class="d-inline-block m-b-0">Notifications</h6>
                            <div class="float-right">
                                {{-- <a href="#!" class="m-r-10">mark as read</a>
                                <a href="#!">clear all</a> --}}
                            </div>
                        </div>
                        <ul class="noti-body dropdown-menu1">
                            {{-- <li class="n-title">
                                <p class="m-b-0">NEW</p>
                            </li> --}}
                            @foreach ($notif as $notif)
                                <a href="{{ url('/admin/vacancy') }}">
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius"
                                                src="{{ asset('uploads/avatar/partner/' . $notif->photo) }}"
                                                alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>{{ substr($notif->name, 0, 13) . '...' }}</strong><span
                                                        class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>{{ date('d-n-Y, h:m:s', strtotime($notif->created_at)) }}</span>
                                                </p>
                                                <p>{{ $notif->pesan }}</p>
                                            </div>
                                        </div>
                                    </li>
                                </a>
                            @endforeach


                        </ul>
                        {{-- <div class="noti-footer">
                            <a href="#!">show all</a>
                        </div> --}}
                    </div>
                </div>
            </li>
        </ul>
    </div>


</header>
<!-- [ Header ] end -->
