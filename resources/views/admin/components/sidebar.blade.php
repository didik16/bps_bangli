<ul class="nav pcoded-inner-navbar ">
    <li class="nav-item pcoded-menu-caption">
        <label>Navigation</label>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.dashboard.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
    </li>
    <li class="nav-item pcoded-menu-caption">
        <label>Master Data</label>
    </li>
    <li class="nav-item {{ Request::segment(2) == 'announcement' ? 'active' : '' }}">
        <a href="{{ route('admin.announcement.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-bell"></i></span><span class="pcoded-mtext">Pengumuman</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) == 'activity' ? 'active' : '' }}">
        <a href="{{ route('admin.activity.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-image"></i></span><span class="pcoded-mtext">Kegiatan</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) == 'position' ? 'active' : '' }}">
        <a href="{{ route('admin.position.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-user-check"></i></span><span class="pcoded-mtext">Posisi</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) == 'vacancy' ? 'active' : '' }}">
        <a href="{{ route('admin.vacancy.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-file-text"></i></span><span class="pcoded-mtext">Lowongan</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) == 'setting' ? 'active' : '' }}">
        <a href="{{ route('admin.setting.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-settings"></i></span><span class="pcoded-mtext">Pengaturan</span></a>
    </li>
    <li class="nav-item pcoded-menu-caption">
        <label>Users</label>
    </li>
    <li class="nav-item {{ Request::segment(3) == 'administrator' ? 'active' : '' }}">
        <a href="{{ route('admin.administrator.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-user"></i></span><span class="pcoded-mtext">Administrator</span></a>
    </li>
    <li class="nav-item {{ Request::segment(3) == 'partner' ? 'active' : '' }}">

        <a href="{{ route('admin.partner.index') }}" class="nav-link "><span class="pcoded-micon"><i
                    class="feather icon-users"></i></span><span class="pcoded-mtext">Mitra</span></a>
    </li>
</ul>
