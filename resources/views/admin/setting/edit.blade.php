@extends('admin.layouts.app')

@section('title')
    Setting Page - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Pengaturan Halaman</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.announcement.index') }}">Pengaturan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Form Pengaturan</h5>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="true">Halaman Profil</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                        aria-controls="profile" aria-selected="false">Tentang Kami</a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                        aria-controls="contact" aria-selected="false">Exampe Tab 3</a>
                                </li> --}}
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                                    <div class="col-md-12 mt-5">
                                        <form action="{{ route('admin.setting.update', $about->id) }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            @method("PUT")
                                            <input type="hidden" name="_type" value="ABOUT_PAGE">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Judul <span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" name="title"
                                                    value="{{ $about->title }}" aria-describedby="emailHelp"
                                                    placeholder="Enter Title">
                                                @error('title')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror

                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Deksripsi Singkat<span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="exampleInputEmail1"
                                                    name="description_short" value="{{ $about->description_short }}"
                                                    aria-describedby="emailHelp" placeholder="Enter Description Short">
                                                @error('description_short')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Deskripsi Panjang<span
                                                        class="text-danger">*</span></label>
                                                <textarea name="description_long"
                                                    id="ckeditor">{{ $about->description_long }}</textarea>
                                                @error('description_long')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Foto Thumbnail <span
                                                        class="text-danger">*</span></label>
                                                <input type="file" name="photo" class="dropify"
                                                    data-default-file="{{ $about->photo }}" />
                                                @error('photo')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                            <a href="{{ route('admin.dashboard.index') }}">
                                                <button type="button" class="btn  btn-secondary">Back</button>
                                            </a>
                                            <button type="submit" class="btn  btn-primary">Update</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    @php $no=1;@endphp
                                    <div class="card-body">
                                        <a class="btn btn-primary mb-3"
                                            href="{{ route('admin.setting.create_about_us') }}">Tambah Data</a>
                                        <table id="datatable" class="table table-striped table-responsive"
                                            style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Thumbnail</th>
                                                    <th>Judul</th>
                                                    <th>Deskripsi</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($about_us as $about_us)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        @isset($about_us->photo)
                                                            <td><img src="{{ asset('/uploads/about_us/' . $about_us->photo) }}"
                                                                    alt=" {{ $about_us->name }}" width="100"></td>
                                                        @else
                                                            <td><img src="https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif"
                                                                    alt="{{ $about_us->name }}" width="100"></td>
                                                        @endisset
                                                        <td>{{ $about_us->title }}</td>
                                                        <td>
                                                            <p
                                                                style="display: -webkit-box;
                                                                                                                                                        -webkit-line-clamp: 2;
                                                                                                                                                        -webkit-box-orient: vertical;
                                                                                                                                                        overflow: hidden;
                                                                                                                                                        text-overflow: ellipsis;
                                                                                                                                                        white-space: nowrap; ">
                                                                {!! $about_us->description ?? '-' !!}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            @if ($about_us->status == 'PUBLISHED')
                                                                <span class="badge badge-primary">Diterbitkan</span>
                                                            @elseif($about_us->status == "UNPUBLISHED")
                                                                <span class="badge badge-secondary">Tidak Diterbitkan</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a
                                                                href="{{ route('admin.setting.edit_about_us', $about_us->id) }}">
                                                                <button class="btn btn-primary">Edit</button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
@endsection
