@extends('admin.layouts.app')

@section('title')
    Data Administrator - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Administrator</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">User</a></li>
                        <li class="breadcrumb-item"><a href="#!">Administrators</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Administrator</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.administrator.create') }}"><span><i
                                                class="feather icon-plus"></i> Add Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Joined At</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $administrator)
                                <tr>
                                    <td><img src="{{ $administrator->photo ?? 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif' }}"
                                            alt="{{ $administrator->name }}" width="100"></td>
                                    <td>{{ $administrator->name }}</td>
                                    <td>{{ $administrator->email }}</td>
                                    <td>{{ date('d F Y - H:i:s', strtotime($administrator->created_at)) }}</td>
                                    <td>
                                        @if ($administrator->status == 'ACTIVE')
                                            <span class="badge badge-primary">Aktif</span>
                                        @elseif($administrator->status == "INACTIVE")
                                            <span class="badge badge-secondary">Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.administrator.edit', $administrator->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
