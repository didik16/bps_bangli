@extends('admin.layouts.app')

@section('title')
    Edit Aktivitas - {{ env("APP_NAME") }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Edit Aktivitas</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i class="feather icon-home"></i></a></li>
                        
                        <li class="breadcrumb-item"><a href="{{ route('admin.activity.index') }}">Aktivitas</a></li>
                        <li class="breadcrumb-item"><a href="#!">Edit Data</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ form-element ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Form Aktivitas</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('admin.activity.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Judul <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="title" value="{{ $data->title }}" aria-describedby="emailHelp" placeholder="Enter Name" >
                                    @error('title')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                          
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Deskripsi <span class="text-danger">*</span></label>
                                    <textarea name="description" id="ckeditor">{{ $data->description  }}</textarea>
                                    @error('description')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status Aktivitas</label>
                                    
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" value="1" class="custom-control-input" id="is-active" @if($data->status == "PUBLISHED") {{ 'checked' }} @endif>
                                        <label class="custom-control-label" for="is-active">Publikasi</label>
                                        @error('status')
                                            <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Photo Thumbnail <span class="text-danger">*</span></label>
                                    <input type="file" name="photo" class="dropify" data-default-file="{{ asset('/uploads/activity/' . $data->photo) }}"/>
                                    @error('photo')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <a href="{{ route('admin.activity.index') }}">
                                    <button type="button" class="btn  btn-secondary">Kembali</button>
                                </a>
                                <button type="submit" class="btn  btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
@endsection