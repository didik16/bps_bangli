@extends('admin.layouts.app')

@section('title')
    Data Kegiatan - {{ env('APP_NAME') }}
@endsection

@section('content')
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Data Kegiatan</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Kegiatan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="card">

                <div class="card-header">
                    <h5>Data Kegiatan</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"><a href="{{ route('admin.activity.create') }}"><span><i
                                                class="feather icon-plus"></i> Tambah Data</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                @php $no=1;@endphp
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $activity)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    @isset($activity->photo)
                                        <td><img src="{{ asset('/uploads/activity/' . $activity->photo) }}"
                                                alt=" {{ $activity->name }}" width="100"></td>
                                    @else
                                        <td><img src="https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif"
                                                alt="{{ $activity->name }}" width="100"></td>
                                    @endisset
                                    <td>
                                        <p style="width: 200px;">{{ $activity->title }}</p>
                                    </td>
                                    <td>
                                        <p style="display: -webkit-box;
                                                                                                    -webkit-line-clamp: 2;
                                                                                                    -webkit-box-orient: vertical;
                                                                                                    overflow: hidden;
                                                                                                    text-overflow: ellipsis;
                                                                                                    white-space: nowrap; ">
                                            {!! $activity->description ?? '-' !!}
                                        </p>
                                    </td>
                                    <td>
                                        @if ($activity->status == 'PUBLISHED')
                                            <span class="badge badge-primary">Diterbitkan</span>
                                        @elseif($activity->status == "UNPUBLISHED")
                                            <span class="badge badge-secondary">Tidak Diterbitkan</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.activity.edit', $activity->id) }}">
                                            <button class="btn btn-primary">Edit</button>
                                        </a>
                                        {{-- <button class="btn btn-danger btn-delete"
                                            data-url="{{ route('admin.activity.destroy', $activity->id) }}">Delete</button>
                                        --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- [ sample-page ] end -->
    </div>
@endsection
