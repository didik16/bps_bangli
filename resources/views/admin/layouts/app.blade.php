
<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('admin/dist/assets/images/favicon.ico') }}" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="{{ asset('admin/dist/assets/css/style.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
    {{-- <link rel="stylesheet" href="{{ asset('home/css/bootstrap-datetimepicker.min.css') }}"> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.css" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap-notifications@1.0.3/dist/stylesheets/bootstrap-notifications.css">

</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
    <!-- [ Pre-loader ] End -->
    
    @include('admin.components.header')
	
	

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        @yield('content')
    </div>
</div>
<!-- [ Main Content ] end -->
    <!-- Warning Section start -->
    <!-- Older IE warning message -->
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->
    <!-- Warning Section Ends -->

   

    <!-- Apex Chart -->
    <script src="{{ asset('admin/dist/assets/js/plugins/apexcharts.min.js') }}"></script>


    <!-- custom-chart js -->
    <script src="{{ asset('admin/dist/assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('home/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/pages/dashboard-main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    
     <!-- Required Js -->
    
     <script src="{{ asset('admin/dist/assets/js/plugins/bootstrap.min.js') }}"></script>
     <script src="{{ asset('admin/dist/assets/js/ripple.js') }}"></script>
     <script src="{{ asset('admin/dist/assets/js/pcoded.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    
    <script>
        $(document).ready( function () {

            let datatable = $('#datatable').DataTable({
                autoWidth: false,
                responsive: true,
            });

            let datatable_partner = $('#datatable_partner').dataTable( {
              "columns": [
                { "width": "90%", "targets": 0 }
                
              ]
            } );

            $('#datatable td p').css('white-space','initial');

            $(".dropify").dropify();

            @if(session()->has('success'))
                swal("Success", "{{ session()->get('success') }}", "success");
            @elseif(session()->has('error'))
                swal("Error", "{{ session()->get('error') }}", "error");
            @endif

            $(".btn-delete").on('click', function(){
         let id = $(this).attr('data-id');
         let url = $(this).attr('data-url');
         swal({
            title: "Confirmation",
            text: "Are you sure want to delete this data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
         })
         .then((willDelete) => {
            let _this = $(this);
         if (willDelete) {
            $.ajaxSetup({
               headers: {
                     'X-CSRF-TOKEN': "{{ csrf_token() }}"
               }
            });

            $.ajax({
                    url: url,
                    type: "DELETE",
                    success: function(response){
                        if(response.success){
                            swal(response.message, {
                                title: "Success",
                                icon: "success",
                            });
                            _this.closest('tr').remove();
                        }
                        else{
                            swal(response.message, {
                                title: "Error",
                                icon: "error",
                            });
                        }
                        
                    },
                        error: function(xhr, status, response){
                            if(status == "error"){
                                swal("Something went wrong", {
                                    title: "Oopps",
                                    icon: "error",
                                });
                            }
                        }
                        })
                    }
                });
            });

            CKEDITOR.replace( 'ckeditor', {
                extraPlugins: 'uploadimage',
                toolbarGroups: [
                    // { name: 'document', groups: [ 'mode' ] },
                    { name: 'basicstyles', groups: [ 'basicstyles' ] },
                    { name: 'paragraph', groups: [ 'list'] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'styles', groups: [ 'styles' ] },
                ],
                removePlugins: 'youtube',
                removeButtons: 'Font,FontSize,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
                filebrowserUploadUrl: "{{route('upload')}}?type=upload&_token={{ csrf_token() }}&path=vacancy",
            });
        } );




        //     $(function(){
        //     var dtToday = new Date();
            
        //     var month = dtToday.getMonth() + 1;
        //     var day = dtToday.getDate();
        //     var year = dtToday.getFullYear();
        //     if(month < 10)
        //         month = '0' + month.toString();
        //     if(day < 10)
        //         day = '0' + day.toString();
            
        //     var maxDate = year + '-' + month + '-' + day;
        //     $('.date').attr('min', maxDate);
        // });


      var notificationsWrapper   = $('.dropdown-notifications');
      var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
      var notificationsCountElem = notificationsToggle.find('i[data-count]');
      var notificationsCount     = parseInt(notificationsCountElem.data('count'));
      var notifications          = notificationsWrapper.find('.dropdown-menu1');

      if (notificationsCount <= 0) {
        // notificationsWrapper.hide();
      }

      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;
         Pusher.logToConsole = true;
      var pusher = new Pusher('138d1aa70b9d14ea0621', {
        encrypted: true
      });

      // Subscribe to the channel we specified in our Laravel Event
      // var channel = pusher.subscribe('status-liked');

    //   var channel = pusher.subscribe('status-liked_'+$('#user_id').val());
      var channel = pusher.subscribe('notif-admin');

      // Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\NotifAdmin', function(data) {
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
       
       var nama_data = data.nama;
       var nama = nama_data.substring(0, 13)+'...';

       var d = new Date(data.waktu);
        var date = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();

        var hours = d.getHours();
        var mins = d.getMinutes();
        var sec = d.getSeconds();

        var datestring = date + "-" + month + "-" + year + " " + hours + ":" + mins + ":" + sec;

        var newNotificationHtml = `
        <a href="{{ url('/admin/vacancy') }}">
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius"
                                                src="{{ asset('uploads/avatar/partner/') }}/`+data.foto+`"
                                                alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>`+nama+`</strong><span
                                                        class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>`+datestring+`</span>
                                                </p>
                                                <p>`+data.message+`</p>
                                            </div>
                                        </div>
                                    </li>
                                </a>`;

        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
      });

    //   $('.dropdown-notifications').on({"hide.bs.dropdown",function() {
    //     console.log('ditutup');
    //     });

      $('.dropdown-notifications').on('hide.bs.dropdown', function () {
          console.log('ditutup');
        notificationsCount = 0;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
    })



    </script>
    @stack('scripts')

    @endstack

    @yield('script')
    
</body>

</html>
