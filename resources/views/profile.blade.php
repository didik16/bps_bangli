@extends('layouts.home-app')
@section('title')
    Profile Saya
@endsection
@section('content')


    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Profile Saya</span> <br><br>
                    <img src="{{ asset('uploads/avatar/partner/' . $data->photo) }}" alt="" width="200px">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="card">
                        <div class="card-header">
                            Kelengkapan Profil Anda
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    @if ($data->detail->completeness == 100)
                                        <div class="alert alert-success">
                                            Bagus! Profil anda sudah lengkap, anda dapat melakukan pelamaran lowongan.
                                        </div>
                                    @else
                                        <div class="alert alert-warning">
                                            Silahkan lengkapi profil anda untuk dapat melakukan pelamaran lowongan.
                                        </div>
                                    @endif
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                            role="progressbar" aria-valuenow="{{ $data->detail->completeness }}"
                                            aria-valuemin="0" aria-valuemax="{{ $data->detail->completeness }}"
                                            style="width: {{ $data->detail->completeness }}%">
                                            {{ $data->detail->completeness }}%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <form action="{{ route('home.profile.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="nama_lengkap">Nama Lengkap</label>
                                <input type="text" name="nama_lengkap" id="nama_lengkap" value="{{ $data->name }}"
                                    class="form-control form-control-lg">
                                @error('nama_lengkap')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="alamat_email">Alamat Email</label>
                                <input type="text" name="alamat_email" id="alamat_email" value="{{ $data->email }}"
                                    class="form-control form-control-lg">
                                @error('alamat_email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" name="alamat" id="alamat" value="{{ $data->detail->address }}"
                                    class="form-control form-control-lg">
                                @error('alamat')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir"
                                    value="{{ $data->detail->birth_place }}" class="form-control form-control-lg">
                                @error('tempat_lahir')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-4 form-group">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                <input type="date" name="tanggal_lahir" id="tanggal_lahir"
                                    value="{{ $data->detail->birth_date }}" class="form-control form-control-lg">
                                @error('tanggal_lahir')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="nomor_telepon">Nomor Telepon</label>
                                <input type="text" name="nomor_telepon" id="nomor_telepon"
                                    value="{{ $data->detail->phone_number }}" class="form-control form-control-lg">
                                @error('nomor_telepon')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="tipe_telepon">Tipe Telepon</label>
                                <input type="text" name="tipe_telepon" id="tipe_telepon"
                                    value="{{ $data->detail->phone_type }}" class="form-control form-control-lg">
                                @error('tipe_telepon')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                <input type="text" name="pendidikan_terakhir" id="pendidikan_terakhir"
                                    value="{{ $data->detail->last_education }}" class="form-control form-control-lg">
                                @error('pendidikan_terakhir')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="aktifitas_terakhir">Kegiatan Terakhir yang Diikuti</label>
                                <input type="text" name="aktifitas_terakhir" id="aktifitas_terakhir"
                                    value="{{ $data->detail->last_activity }}" class="form-control form-control-lg">
                                @error('aktifitas_terakhir')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="status">Status</label>
                                <div class="form-check">
                                    <input type="checkbox" name="status" value="1" class="form-check-input" id="status"
                                        @if ($data->detail->is_marriage == 1) checked
                                    @endif>
                                    <label class="form-check-label" for="status">Saya sudah menikah</label>
                                </div>
                                @error('status')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="ktp">KTP</label>
                                <input type="file" name="ktp" class="dropify"
                                    data-default-file="{{ asset('uploads/partner/files/'.$data->detail->ktp) }}" />
                                @error('ktp')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="ijazah">Ijazah</label>
                                <input type="file" name="ijazah" class="dropify"
                                    data-default-file="{{ asset('uploads/partner/files/'.$data->detail->ijazah) }}" />
                                @error('ijazah')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="kartu_keluarga">Kartu Keluarga</label>
                                <input type="file" name="kartu_keluarga" class="dropify"
                                    data-default-file="{{ asset('uploads/partner/files/'.$data->detail->kk) }}" />
                                @error('kartu_keluarga')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-12">
                                <input type="submit" value="Update Profil" class="btn btn-primary btn-lg px-5">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <h2>Tunggu Apalagi?</h2>
                    <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                </div>
                <div class="col-lg-5 d-flex justify-content-end">
                    <button class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi Mitra</button>
                </div>
            </div>
        </div>
    </div>

@endsection
