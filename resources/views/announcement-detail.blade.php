@extends('layouts.home-app')
@section('title')
    {{ $data->title }}
@endsection
@section('content')


    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Pengumuman</span>
                    <h2 class="title-with-line text-center mb-5">{{ $data->title }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{ asset('/uploads/announcement/' . $data->photo) }}" alt="Image" class="img-fluid"
                        width="250">
                </div>
                <div class="col-md-12 mt-5 mb-5">
                    {!! $data->description !!}
                </div>
            </div>
        </div>
    </div>


    <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <h2>Tunggu Apalagi?</h2>
                    <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                </div>
                <div class="col-lg-5 d-flex justify-content-end">
                    <button class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi Mitra</button>
                </div>
            </div>
        </div>
    </div>

@endsection
