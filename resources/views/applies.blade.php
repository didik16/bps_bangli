@extends('layouts.home-app')
@section('title')
    Lamaran Saya
@endsection
@section('content')


    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Lamaran Saya</span> <br><br>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="card">
                        <div class="card-header">
                            Data Lamaran saya
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="datatable" class="table table-striped" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Nama Lowongan</th>
                                                <th>Posisi</th>
                                                <th>Tanggal Melamar</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (isset($data))
                                                @foreach ($data as $apply)
                                                    <tr>
                                                        <td>
                                                            <a
                                                                href="{{ route('home.vacancy.detail', $apply->vacancy_id) }}">
                                                                {{ $apply->vacancy->title }}
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <span
                                                                class="badge badge-primary">{{ $apply->position->name }}</span>
                                                        </td>
                                                        <td>{{ date('d F Y - H:i', strtotime($apply->created_at)) }}</td>
                                                        <td>
                                                            @if ($apply->status == 'UNAPPROVED')
                                                                <span class="badge badge-warning text-white">Belum
                                                                    Disetujui</span>
                                                            @elseif($apply->status == "APPROVED")
                                                                <span
                                                                    class="badge badge-success text-white">Disetujui</span>
                                                            @elseif($apply->status == "INTERVIEW")
                                                                <span class="badge badge-info text-white">Interview</span>
                                                            @else
                                                                <span class="badge badge-danger">Ditolak</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($apply->status == 'UNAPPROVED')
                                                                <a href="{{ route('home.applies.cancel', $apply->id) }}">
                                                                    <button type="button"
                                                                        class="btn btn-sm btn-danger">Batalkan</button>
                                                                </a>
                                                            @elseif ($apply->status == 'INTERVIEW')
                                                                <button type="button" class="btn btn-sm btn-danger"
                                                                    disabled>Batalkan</button>
                                                            @elseif ($apply->status == 'REJECTED')
                                                                <a href="{{ url('/account/applies/' . $apply->id) }}/detail"
                                                                    class="btn btn-sm btn-danger">Detail</a>
                                                            @else
                                                                <a href="{{ url('/account/applies/' . $apply->id) }}/detail"
                                                                    class="btn btn-sm btn-success"
                                                                    style="color: white">Detail</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <p>belum ada data</p>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @guest

        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>
    @endguest



@endsection
