@extends('layouts.home-app')
@section('title')
    Tentang Kami
@endsection
@section('content')

    <style>
        .foto-responsive {
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            padding-bottom: 56%;
            min-width: 100%;
        }

    </style>

    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Tentang Kami</span>
                    <h2 class="title-with-line text-center mb-5">{{ $about->title ?? '-' }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{ url($about->photo) }}" alt="Image" class="img-fluid" width="250">
                </div>
                <div class="col-md-12 mt-5 mb-5">
                    {!! $about->description_long ?? '-' !!}
                </div>
            </div>
            <div class="row">
                @if (count($about_us) > 0)
                    <section class=" cards-gallery">
                        <div class="container">
                            <div class="row mb-5">
                                @foreach ($about_us as $about_us)
                                    <div class="col-md-4">
                                        <div class="card border-0 transform-on-hover">
                                            <a class="lightbox" href="{{ asset('/uploads/about_us/' . $about_us->photo) }}">

                                                <div class="box-img foto-responsive card-img-top"
                                                    alt="<h3>{{ $about_us->title }}</h3>{{ $about_us->description }}""
                                                                                                                                            style="
                                                    background-image: url(
                                                    {{ asset('/uploads/about_us/' . $about_us->photo) }} );">
                                                </div>

                                                <img src="{{ asset('/uploads/about_us/' . $about_us->photo) }}"
                                                    class="card-img-top"
                                                    alt="<h3>{{ $about_us->title }}</h3>{{ $about_us->description }}"
                                                    style="width:0%">

                                            </a>
                                            <div class="card-body">
                                                <h6><a href="#">{{ $about_us->title }}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                @else
                    <div class="col-lg-12 text-center">
                        <p>Tidak ada info kegiatan untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>



    @guest
        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>
    @endguest

@endsection
