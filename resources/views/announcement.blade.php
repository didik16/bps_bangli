@extends('layouts.home-app')

@section('title')
    Pengumuman
@endsection

@section('content')

    <style>
        .foto-responsive {
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            padding-bottom: 80%;
            min-width: 100%;
        }

    </style>

    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Pengumuman</span>
                    <h2 class="title-with-line text-center mb-5">Pengumuman Terbaru Dari Kami</h2>
                </div>
            </div>
            <div class="row mb-5">
                @if (count($announcements) > 0)
                    @foreach ($announcements as $announcement)
                        <div class="col-lg-4">
                            <div class="news-entry-item">
                                <a href="{{ route('home.announcement.detail', $announcement->id) }}" class="thumbnail">
                                    {{-- <img src="{{ $announcement->photo }}" alt="Image"
                                        class="img-fluid"> --}}
                                    {{-- <img
                                        src="{{ asset('/uploads/announcement/' . $announcement->photo) }}" alt="Image"
                                        class="img-fluid"> --}}

                                    <div class="box-img foto-responsive card-img-top"
                                        style="
                                                                        background-image: url(
                                                                        {{ asset('/uploads/announcement/' . $announcement->photo) }} );">
                                    </div>
                                    <div class="date">
                                        <span>{{ date('d', strtotime($announcement->created_at)) }}</span>
                                        <span>{{ date('F', strtotime($announcement->created_at)) }}</span>
                                    </div>
                                </a>
                                <h3 class="mb-0"><a href="#">{{ $announcement->title }}</a></h3>
                                <div class="mb-3">
                                    by <b>{{ $announcement->administrator->name ?? '-' }}</b>
                                </div>
                                <p align="justify">
                                    {{ substr(strip_tags($announcement->description), 0, 200) }}..
                                </p>
                            </div>
                        </div>
                    @endforeach
                @else

                    <div class="col-lg-12 text-center">
                        <p>Tidak ada pengumuman untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @guest
        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>
    @endguest


@endsection
