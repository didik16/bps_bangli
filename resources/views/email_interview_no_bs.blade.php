<!DOCTYPE html>
<html>

<head>
    <title>BPS Bangli - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">
* {
    box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: block;
}
[class*="col-"] {
    float: left;
    padding: 15px;
}

/* For mobile phones: */
[class*="col-"] {
    width: 100%;
}
@media only screen and (min-width: 600px) {
    /* For tablets: */
    .col-m-1 {width: 8.33%;}
    .col-m-2 {width: 16.66%;}
    .col-m-3 {width: 25%;}
    .col-m-4 {width: 33.33%;}
    .col-m-5 {width: 41.66%;}
    .col-m-6 {width: 50%;}
    .col-m-7 {width: 58.33%;}
    .col-m-8 {width: 66.66%;}
    .col-m-9 {width: 75%;}
    .col-m-10 {width: 83.33%;}
    .col-m-11 {width: 91.66%;}
    .col-m-12 {width: 100%;}
}
@media only screen and (min-width: 768px) {
    /* For desktop: */
    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 25%;}
    .col-4 {width: 33.33%;}
    .col-5 {width: 41.66%;}
    .col-6 {width: 50%;}
    .col-7 {width: 58.33%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%;}
    .col-12 {width: 100%;}
}
img {
    width: 100%;
    height: auto;
}

body {
background-image:url('bg.png');
  font-family: Tahoma,Geneva,sans-serif;
}

#wrap {
    width:80%;
    margin:auto;
    background-color:white;
    /*border-radius: 10px;*/
    padding: 5px;
/*    -moz-box-shadow: 0px 0px 10px #000;
    -webkit-box-shadow: 0px 0px 10px #000;
    box-shadow: 0px 0px 10px #000;*/
}


</style>

</head>
<body style="background: white">
    <div id="wrap" >
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-sm-12 mt-5"  style="background: white;padding-bottom: 15px;">
                <img src="{{ asset('home/images/bps-logo.png') }}" width="100%">
                    <center><h2 class="mt-3 mb-3">PENGUMUMAN HASIL SELEKSI LOWONGAN</h2></center>
                <div class="row justify-content-center">
                     <div class="" style="width:30%;float: left;padding: 10px">
                        <div class="box-img" style="background-repeat: no-repeat;background-position: center center;background-size: cover;padding-bottom: 120%;min-width: 100%;background-image: url( https://kpu-kotabatu.go.id/wp-content/uploads/3.-FOTO-3X4-HUDIN.jpg );">
                        </div>
                    </div>
                     <div class="" style="width:70%;float: right;padding: 10px">
                            <table style="width: 100%;height:100%;background: #f6f6f6;padding: 5px 10px">
                                <tbody>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">ID Peserta</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $id }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Nama Lengkap</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $nama }}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 25%;padding: 5px 10px">Tanggal Lahir</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $tgl }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Alamat</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $alamat }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">No HP</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $hp }}</td>
                                    </tr>
                                    <tr>
                                        @if ($status == 'lulus')
                                            <td colspan="3"
                                                style="padding-top: 12px;padding-left: 5px;font-weight: bolder">
                                                Selamat! Anda dinyatakan
                                                <span style="background: green;color: white;padding: 5px 10px">Lulus
                                                    Seleksi</span>
                                            </td>
                                        @else
                                            <td colspan="3"
                                                style="padding-top: 12px;padding-left: 5px;font-weight: bolder">
                                                Maaf, Anda dinyatakan
                                                <span style="background: red;color: white;padding: 5px 10px">Tidak Lulus
                                                    Seleksi</span>
                                            </td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Lowongan</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $lowongan }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Posisi</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $posisi }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;padding: 5px 10px">Nilai</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 75%;font-weight: bolder;">{{ $nilai }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--
@endforeach --}}

</body>

</html>
