@extends('layouts.home-app')

@section('title')
    Beranda
@endsection

@section('hero')
    @include('components.hero')
@endsection

@section('content')

    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-4 mb-lg-0">
                    <img src="{{ url($about->photo) }}" alt="Image" class="img-fluid">
                </div>
                <div class="col-lg-5 ml-auto">
                    <span class="caption">Tentang Kami</span>
                    <h2 class="title-with-line">{{ $about->title ?? '-' }}</h2>


                    <p class="mb-4">
                        {{ $about->description_short ?? '-' }}
                    </p>

                    <a href="{{ route('home.about') }}">
                        <button class="btn btn-outline-primary">Lihat Selengkapnya</button>
                    </a>


                </div>
            </div>
        </div>
    </div>

    <div class="site-section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <div class="numbers">
                        <strong class="d-block">{{ $count_partner }}</strong>
                        <span>Jumlah Mitra</span>
                    </div>
                </div>
                <div class="col-lg-6 text-center">
                    <div class="numbers">
                        <strong class="d-block">{{ $count_vacancy }}</strong>
                        <span>Jumlah Lowongan</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light pb-0">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Lowongan</span>
                    <h2 class="title-with-line text-center mb-5">Ayo ikuti lowongan yang tersedia dari kami</h2>
                </div>
            </div>
            <div class="row">
                @if (count($vacancies) > 0)

                    @foreach ($vacancies as $vacancy)

                        @if (Carbon\Carbon::now()->format('Y-m-d h:m:s') < $vacancy->end_period)

                            <div class="col-lg-4 col-md-6">
                                <div class="feature-1">
                                    <div class="feature-1-content">
                                        <img src="{{ asset('/uploads/vacancy/' . $vacancy->photo) }}" class="mb-5" alt=""
                                            width="100%">
                                        <h2>{{ $vacancy->title }}</h2>
                                        <p align="justify">{{ substr(strip_tags($vacancy->description), 0, 200) }}..</p>
                                        <p><a href="{{ route('home.vacancy.detail', $vacancy->id) }}"
                                                class="btn btn-outline-primary px-4 ">Lihat Selengkapnya</a></p>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td align="left">Berakhir Pada</td>
                                                <td align="left">:</td>
                                                <td align="left">
                                                    <span class="badge badge-primary">
                                                        {{ date('d F Y', strtotime($vacancy->end_period)) }}
                                                    </span>
                                                </td>
                                            </tr>
                                            @foreach ($vacancy->detail as $detail)
                                                <tr>
                                                    <td align="left">Posisi {{ $detail->position->name }}</td>
                                                    <td align="left">:</td>
                                                    <td align="left">
                                                        <span class="badge badge-success text-white">
                                                            Diperlukan {{ $detail->max_partner }}, Tersedia
                                                            {{ $detail->max_partner - $detail->current_partner }},
                                                            Terisi
                                                            {{ $detail->max_partner }}
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                    <div class="col-lg-12 mb-5 d-flex justify-content-end">
                        <a href="{{ route('home.vacancy') }}">
                            <button class="btn btn-primary">Lihat Lebih Banyak</button>
                        </a>
                    </div>

                @else
                    <div class="col-lg-12 text-center">
                        <p>Tidak ada lowongan untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Info Kegiatan</span>
                    <h2 class="title-with-line text-center mb-5">Info Kegiatan Kami</h2>
                </div>
            </div>
            <div class="row">
                @if (count($activities) > 0)

                    <section class="gallery-block cards-gallery">
                        <div class="container">
                            <div class="row">
                                @foreach ($activities as $activity)
                                    <div class="col-md-6 col-lg-4">
                                        <div class="card border-0 transform-on-hover">
                                            <a class="lightbox" href="{{ asset('/uploads/activity/' . $activity->photo) }}">
                                                <img src="{{ asset('/uploads/activity/' . $activity->photo) }}" alt="<h3>{{ $activity->title }}</h3>{{ $activity->description }}
                                                                                                  " class="card-img-top">
                                            </a>
                                            <div class="card-body">
                                                <h6><a href="#">{{ $activity->title }}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="col-lg-12 mb-5 d-flex justify-content-end">
                                    <a href="{{ route('home.activity') }}">
                                        <button class="btn btn-primary">Lihat Lebih Banyak</button>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </section>
                @else
                    <div class="col-lg-12 text-center">
                        <p>Tidak ada info kegiatan untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>




    <div class="site-section">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 mb-5">
                    <span class="caption">Pengumuman</span>
                    <h2 class="title-with-line mb-2">Pengumuman Terbaru Dari Kami</h2>
                </div>
            </div>

            <div class="row">
                @if (count($announcements) > 0)
                    @foreach ($announcements as $announcement)
                        <div class="col-lg-4">
                            <div class="news-entry-item">
                                <a href="{{ route('home.announcement.detail', $announcement->id) }}" class="thumbnail">
                                    <img src="{{ asset('/uploads/announcement/' . $announcement->photo) }}" alt="Image"
                                        class="img-fluid">
                                    <div class="date">
                                        <span>{{ date('d', strtotime($announcement->created_at)) }}</span>
                                        <span>{{ date('F', strtotime($announcement->created_at)) }}</span>
                                    </div>
                                </a>
                                <h3 class="mb-0"><a href="#">{{ $announcement->title }}</a></h3>
                                <div class="mb-3">
                                    by <b>{{ $announcement->administrator->name ?? '-' }}</b>
                                </div>
                                <p align="justify">
                                    {{ substr(strip_tags($announcement->description), 0, 200) }}..
                                </p>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-lg-12 mb-5 mt-5 d-flex justify-content-end">
                        <a href="{{ route('home.announcement') }}">
                            <button class="btn btn-primary">Lihat Lebih Banyak</button>
                        </a>
                    </div>
                @else

                    <div class="col-lg-12 text-center">
                        <p>Tidak ada pengumuman untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @guest
        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>

    @endguest

@endsection
