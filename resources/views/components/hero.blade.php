<div class="hero-slide owl-carousel site-blocks-cover">
    <div class="intro-section" style="background-image: url('{{ asset('home/images/hero_1.jpg') }}');">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7 mx-auto text-center" data-aos="fade-up">
                    <h1>Badan Pusat Statistik Kabupaten Bangli</h1>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas, earum quibusdam porro deserunt
                        voluptas.</p>
                    @guest
                        <p><a href="/register" class="btn btn-primary">Bergabung Menjadi
                                Mitras</a></p>
                    @endguest
                </div>
            </div>
        </div>
    </div>

</div>
