@if (Auth::guard('partners')->check())
    <input type="hidden" id="user_id" value="{{ \Auth::guard('partners')->user()->id }}">
@endif
<div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>

<header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

    <div class="container">
        <div class="d-flex align-items-center ">
            <div class="site-logo">
                <a href="{{ route('home.index') }}" class="d-block">
                    <img src="{{ asset('home/images/logo_bps.png') }}" alt="Image" class="img-fluid" width="200">
                </a>
            </div>
            <div class="ml-auto">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                        <li @if (Request::segment(1) == '') class="active"
                            @endif>
                            <a href="{{ route('home.index') }}" class="nav-link text-left">Beranda</a>
                        </li>
                        <li @if (Request::segment(1) == 'about') class="active"
                            @endif>
                            <a href="{{ route('home.about') }}" class="nav-link text-left">Tentang Kami</a>
                        </li>
                        <li @if (Request::segment(1) == 'vacancy') class="active"
                            @endif>
                            <a href="{{ route('home.vacancy') }}" class="nav-link text-left">Lowongan</a>
                        </li>
                        <li @if (Request::segment(1) == 'announcement') class="active"
                            @endif>
                            <a href="{{ route('home.announcement') }}" class="nav-link text-left">Pengumuman</a>
                        </li>
                        <li @if (Request::segment(1) == 'activity') class="active"
                            @endif>
                            <a href="{{ route('home.activity') }}" class="nav-link text-left">Info Kegiatan</a>
                        </li>
                        @if (\Auth::guard('administrators')->check())
                            <li>
                                <a href="{{ route('admin.dashboard.index') }}" class="nav-link text-left">Dashboard</a>
                            </li>
                        @elseif(\Auth::guard('partners')->check())

                            <li class="has-children">
                                <a href="javascript:void(0)" class="nav-link text-left">Akun</a>
                                <ul class="dropdown">
                                    <li><a href="{{ route('home.profile') }}"">Profil Saya</a></li>
                                    <li><a href=" {{ route('home.applies') }}">Lamaran Saya</a></li>
                                    <li><a href="{{ route('logout') }}" class="nav-link text-left">Logout</a></li>
                                </ul>
                            </li>

                            @isset($notif)
                                <li>
                                    <div class="dropdown dropdown-notifications">
                                        <a href="" id="dropdownMenuButton" class="dropdown-toggle nav-link text-left"
                                            data-toggle="dropdown">
                                            <i class="far fa-bell notification-icon" data-count="0"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"
                                            style="width: 300px !important;">
                                            <div class="col-md-12 mt-3 dropdown-menu1">
                                                @foreach ($notif as $notif)
                                                    <div class="row mb-3">
                                                        <div class="col-md-3">
                                                            <img src="https://www.sunsetlearning.com/wp-content/uploads/2019/09/User-Icon-Grey-300x300.png"
                                                                class="img-circle" alt="50x50"
                                                                style="width: 50px; height: 50px;">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <strong class="notification-title">{{ $notif->pesan }}</strong>
                                                            <!--p class="notification-desc">Extra description can go here</p-->
                                                            <div class="notification-meta">
                                                                <small
                                                                    class="timestamp">{{ date('d F Y, h:m:s', strtotime($notif->created_at)) }}</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endisset

                        @else
                            <li>
                                <a href="{{ route('login.index') }}" class="nav-link text-left">Masuk</a>
                            </li>
                            <li>
                                <a href="{{ route('register.index') }}" class="nav-link text-left">Daftar</a>
                            </li>
                        @endif
                    </ul>
                </nav>

            </div>

        </div>
    </div>

</header>
