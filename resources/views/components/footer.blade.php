<div class="footer">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-lg-3">
          <p><img src="https://banglikab.bps.go.id/backend/images/Header-Frontend-Besar-ind.png" alt="Image" class="img-fluid"></p>
          
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="copyright">
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>