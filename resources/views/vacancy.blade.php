@extends('layouts.home-app')

@section('title')
    Lowongan
@endsection

@section('content')


    <style>
        .foto-responsive {
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            padding-bottom: 80%;
            min-width: 100%;
        }

    </style>

    <div class="site-section bg-light pb-0" style="padding-top:250px;">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-4">
                    <span class="caption">Lowongan</span>
                    <h2 class="title-with-line text-center mb-5">Ayo ikuti lowongan yang tersedia dari kami</h2>
                </div>
            </div>
            <div class="row">
                @if (count($vacancies) > 0)

                    @foreach ($vacancies as $vacancy)

                        @if (Carbon\Carbon::now()->format('Y-m-d h:m:s') < $vacancy->end_period)

                            <div class="col-lg-4 col-md-6">
                                <div class="feature-1">
                                    <div class="feature-1-content">
                                        {{-- <img
                                            src="{{ asset('/uploads/vacancy/' . $vacancy->photo) }}" class="mb-5" alt=""
                                            width="100%"> --}}
                                        <div class="foto-responsive mb-3"
                                            style="
                                                                                                                            background-image: url(
                                                                                                                            {{ asset('/uploads/vacancy/' . $vacancy->photo) }} );">
                                        </div>
                                        <h2>{{ $vacancy->title }}</h2>
                                        <p align="justify">{{ substr(strip_tags($vacancy->description), 0, 200) }}..</p>
                                        <p><a href="{{ route('home.vacancy.detail', $vacancy->id) }}"
                                                class="btn btn-outline-primary px-4 ">Lihat Selengkapnya</a></p>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td align="left">Berakhir Pada</td>
                                                <td align="left">:</td>
                                                <td align="left">
                                                    <span class="badge badge-primary">
                                                        {{ date('d F Y', strtotime($vacancy->end_period)) }}
                                                    </span>
                                                </td>
                                            </tr>
                                            @if (isset($vacancy->detail))
                                                @foreach ($vacancy->detail as $detail)
                                                    <tr>
                                                        <td align="left">Posisi {{ $detail->position->name }}</td>
                                                        <td align="left">:</td>
                                                        <td align="left">
                                                            <span class="badge badge-success text-white">
                                                                Diperlukan {{ $detail->max_partner }}, Tersedia
                                                                {{ $detail->max_partner - $detail->current_partner }},
                                                                Terisi
                                                                {{ $detail->max_partner }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>

                        @else
                            <div class="col-lg-12 text-center">
                                {{-- <p>Tidak ada lowongan untuk saat ini</p>
                                --}}
                            </div>
                        @endif

                    @endforeach

                @else
                    <div class="col-lg-12 text-center">
                        <p>Tidak ada lowongan untuk saat ini</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @guest
        <div class="site-section ftco-subscribe-1" style="background-image: url('{{ asset('home/images/hero_2.jpg') }}')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h2>Tunggu Apalagi?</h2>
                        <p>Saatnya sekarang juga anda bergabung menjadi mitra kami.</p>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-end">
                        <a href="/register" class="btn btn-outline-light rounded py-3 px-4" type="submit">Bergabung Menjadi
                            Mitra</a>
                    </div>
                </div>
            </div>
        </div>
    @endguest


@endsection
