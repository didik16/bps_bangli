
<!DOCTYPE html>
<html lang="en">

<head>

	<title>Daftar Mitra- {{ env("APP_NAME") }}</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="{{ asset('admin/dist/assets/images/favicon.ico') }}" type="image/x-icon">

	<!-- vendor css -->
	<link rel="stylesheet" href="{{ asset('admin/dist/assets/css/style.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
	
	


</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper" style="background: #f8f9fd;">
	<div class="auth-content" style="width: 600px">
		<div class="card">
			<div class="row align-items-center text-center">
				<div class="col-md-12">
					<div class="card-body">
						<a href="{{ route('home.index') }}">
                            <img src="https://banglikab.bps.go.id/backend/images/Header-Frontend-Besar-ind.png" alt="" class="img-fluid mb-4">
                        </a>
						<h4 class="mb-3 f-w-400">Daftar Mitra</h4>
						<form action="{{ route('register.store') }}" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="form-group mb-3">
								<label class="floating-label" for="name">Nama Lengkap</label>
								<input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="">
								@error('name')
									<small class="form-text text-danger" style="text-align: left">{{ $message }}</small>
								@enderror
                            </div>
                            <div class="form-group mb-3">
								<label class="floating-label" for="Email">Alamat Email</label>
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" id="Email" placeholder="">
								@error('email')
									<small class="form-text text-danger" style="text-align: left">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group mb-4">
								<label class="floating-label" for="Password">Kata Sandi</label>
								<input type="password" class="form-control" name="password" id="Password" placeholder="">
								@error('password')
									<small class="form-text text-danger" style="text-align: left">{{ $message }}</small>
								@enderror
                            </div>
                            <div class="form-group mb-4">
								<label class="floating-label" for="password_confirmation">Ulangi Kata Sandi</label>
								<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="">
								@error('password')
									<small class="form-text text-danger" style="text-align: left">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group mb-4 text-left">
								<div class="form-group">
                                    <label for="exampleInputPassword1" class="text-left">Foto Profil</label>
                                    <input type="file" name="photo" class="dropify" />
                                    @error('photo')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
							</div>
							<button class="btn btn-block btn-primary mb-4">Daftar</button>
						</form>
						<p class="mb-0 text-muted">Sudah memiliki akun? <a href="{{ route('login.index') }}" class="f-w-400">Login</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="{{ asset('admin/dist/assets/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/ripple.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/pcoded.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script>
	$(document).ready(function(){
		$(".dropify").dropify();
		@if(session()->has('success'))
                swal("Success", "{{ session()->get('success') }}", "success");
		@elseif(session()->has('error'))
			swal("Error", "{{ session()->get('error') }}", "error");
		@endif
	})
</script>



</body>

</html>
