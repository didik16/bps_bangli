<!DOCTYPE html>
<html lang="en">

<head>
  <title>@yield('title') - {{ env('APP_NAME') }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('home/fonts/icomoon/style.css') }}">

  <link rel="stylesheet" href="{{ asset('home/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('home/css/jquery-ui.css') }}">
  <link rel="stylesheet" href="{{ asset('home/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('home/css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('home/css/owl.theme.default.min.css') }}">

  <link rel="stylesheet" href="{{ asset('home/css/jquery.fancybox.min.css') }}">

  <link rel="stylesheet" href="{{ asset('home/css/bootstrap-datepicker.css') }}">

  <link rel="stylesheet" href="{{ asset('home/fonts/flaticon/font/flaticon.css') }}">

  <link rel="stylesheet" href="{{ asset('home/css/aos.css') }}">
  <link href="{{ asset('home/css/jquery.mb.YTPlayer.min.css') }}" media="all" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="{{ asset('home/css/style.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap-notifications@1.0.3/dist/stylesheets/bootstrap-notifications.css">
  {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}

  <script src="https://kit.fontawesome.com/142f9408d2.js" crossorigin="anonymous"></script>
  <style>

    #baguetteBox-overlay .full-image figcaption{
      padding: 2em !important;
    }
    #baguetteBox-overlay .full-image figcaption h3{
      color: white;
    }
   
    .counter.counter-lg {
    top: -24px !important;
    }

    .dot {
        height: 15px;
        width: 15px;
        background-color: red;
        border-radius: 50%;
        display: inline-block;
      }

    .sec{
        position: relative;
        right: -13px;
        top:-22px;
    }

  </style>
  



</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" style="margin-top: -100px">

    <div class="site-wrap">

        @include('components.header')


        @yield('hero')
        @yield('content')


        @include('components.footer')


    </div>
    <!-- .site-wrap -->


    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#4680ff"/></svg></div>

    <script src="{{ asset('home/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('home/js/popper.min.js') }}"></script>
    <script src="{{ asset('home/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('home/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('home/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('home/js/aos.js') }}"></script>
    <script src="{{ asset('home/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('home/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('home/js/jquery.mb.YTPlayer.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    

    
    
    <script src="{{ asset('home/js/main.js') }}"></script>
    
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>

    <script type="text/javascript">
      var notificationsWrapper   = $('.dropdown-notifications');
      var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
      var notificationsCountElem = notificationsToggle.find('i[data-count]');
      var notificationsCount     = parseInt(notificationsCountElem.data('count'));
      var notifications          = notificationsWrapper.find('.dropdown-menu1');

      if (notificationsCount <= 0) {
        // notificationsWrapper.hide();
      }

      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;
         Pusher.logToConsole = true;
      var pusher = new Pusher('138d1aa70b9d14ea0621', {
        encrypted: true
      });

      // Subscribe to the channel we specified in our Laravel Event
      // var channel = pusher.subscribe('status-liked');

      var channel = pusher.subscribe('status-liked_'+$('#user_id').val());

      // Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\StatusLiked', function(data) {
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
       

        var newNotificationHtml = `
        <div class="row mb-3">
          <div class="col-md-3">
            <img src="https://www.sunsetlearning.com/wp-content/uploads/2019/09/User-Icon-Grey-300x300.png" class="img-circle" alt="50x50" style="width: 50px; height: 50px;">
          </div>
          <div class="col-md-9">
            <strong class="notification-title">`+data.message+`</strong>
          <!--p class="notification-desc">Extra description can go here</p-->
          <div class="notification-meta">
            <small class="timestamp">about a minute ago</small>
          </div>
          </div>
        </div>
        `;
        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
      });

      $('.dropdown-notifications').on('hide.bs.dropdown', function () {
        console.log('ditutup');

        notificationsCount = 0;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
    })
    </script>

    <script>
      $(document).ready(function(){
        $(".dropify").dropify();

        let datatable = $('#datatable').DataTable({
                autoWidth: true,
            });

        @if(session()->has('success'))
                swal("Success", "{{ session()->get('success') }}", "success");
            @elseif(session()->has('error'))
                swal("Error", "{{ session()->get('error') }}", "error");
            @endif

        baguetteBox.run('.cards-gallery', { 
          animation: 'slideIn',
          captions: function(element) {
            return element.getElementsByTagName('img')[0].alt;
          }
        });
      })
    </script>
  </body>

  </html>