-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2020 at 09:13 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bps_bangli`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `administrator_id`, `title`, `description`, `photo`, `status`, `created_at`, `updated_at`) VALUES
('e9f44863-293d-42bf-b2bd-f2d580f3f5b9', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'Sensus Penduduk 2020', 'wadwawawd', '1603436009.jpeg', 'PUBLISHED', '2020-10-23 06:53:29', '2020-10-23 06:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `name`, `email`, `password`, `photo`, `created_at`, `updated_at`) VALUES
('4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'Admin Master', 'admin@gmail.com', '$2y$10$hEDNJrCVln4tWUxME0/HC.QtFRf7M09M3fM2H9QnctwOwbadHhZ.2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `administrator_id`, `title`, `description`, `status`, `photo`, `created_at`, `updated_at`) VALUES
('c06a1919-16f6-46de-81f3-40d0b00db2d3', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'sakernas', '<p>aaasda</p>', 'PUBLISHED', '1603437769.png', '2020-10-23 07:22:49', '2020-10-23 07:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE `interviews` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_applies_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `pewawancara` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interviews`
--

INSERT INTO `interviews` (`id`, `vacancy_applies_id`, `vacancy_id`, `position_id`, `partner_id`, `tanggal`, `pewawancara`, `nilai`, `created_at`, `updated_at`) VALUES
('2c9399c7-e791-4602-a65e-62becf7a867d', '4bca8a76-c5e9-4445-afde-946ed92ed8ea', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', '2020-12-01 00:35:00', 'Andi', 80, '2020-11-29 00:35:24', '2020-12-02 11:08:54'),
('ac9399c7-e791-4602-a65e-62becf7a867s', '2d4cb262-fe4c-4b37-aba4-912bec931a79', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', '2020-12-01 00:35:00', 'Andi', 67, '2020-11-29 00:35:24', '2020-12-02 00:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(47, '2014_10_12_000000_create_users_table', 1),
(48, '2014_10_12_100000_create_password_resets_table', 1),
(49, '2020_10_12_070435_create_administrators_table', 1),
(50, '2020_10_12_183418_create_partners_table', 1),
(51, '2020_10_13_091515_create_vanancies_table', 1),
(52, '2020_10_13_140119_create_announcements_table', 1),
(53, '2020_10_13_142213_create_galleries_table', 1),
(54, '2020_10_13_144235_create_partner_details_table', 1),
(55, '2020_10_14_114032_create_settings_table', 1),
(56, '2020_10_14_132045_create_positions_table', 1),
(57, '2020_10_14_135736_add_column_description_on_galleries_table', 2),
(58, '2020_10_14_141108_create_activities_table', 2),
(59, '2020_10_14_170523_create_vacancy_details_table', 3),
(60, '2020_10_14_171824_delete_column_max_participant', 3),
(61, '2020_10_21_203339_create_vacancy_applies_table', 3),
(69, '2020_11_21_124450_create_interviews_table', 4),
(71, '2020_11_23_071218_create_notifications_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `partner_id`, `pesan`, `created_at`, `updated_at`) VALUES
('019654c6-b9db-4275-b664-688c147e471d', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Selamat anda Lolos!', '2020-12-02 11:34:16', '2020-12-02 11:34:16'),
('0d2f0e02-6a3f-489b-b918-d7c6d28df64b', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Selamat anda lolos tahap Administrasi', '2020-11-23 00:07:49', '2020-11-23 00:07:49'),
('6f943095-23c3-4736-8059-2b2371bbf89c', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Selamat anda Lolos!', '2020-12-02 11:38:22', '2020-12-02 11:38:22'),
('7d2f0e02-6a3f-489b-b918-d7c6d28df64b', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Selamat anda lolos tahap Administrasi', '2020-11-23 00:07:49', '2020-11-23 00:07:49'),
('7d2f9e02-6a3f-489b-b918-d7c6d28df64b', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Melamar Lowongan Sensus Penduduk 2020 Posisi Entri Data', '2020-11-23 00:07:49', '2020-11-23 00:07:49'),
('ade80af3-7d23-4f9e-a0b7-e88701075e3a', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Melamar Lowongan Sensus Penduduk 2020 Posisi Entrie Data', '2020-12-07 03:17:23', '2020-12-07 03:17:23'),
('d0bd6c17-730c-4b27-8087-9920fa3f75a7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Melamar Lowongan Sensus Penduduk 2020 Posisi Pengawas', '2020-12-07 03:15:18', '2020-12-07 03:15:18'),
('f2bd8412-cf75-4bf6-9985-64c33b676fc7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Melamar Lowongan Sensus Penduduk 2020 Posisi Pencacah', '2020-12-07 03:18:51', '2020-12-07 03:18:51');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `email`, `password`, `photo`, `status`, `created_at`, `updated_at`) VALUES
('434252bf-1814-481a-a6a4-e521cfa7f4d4', 'xxxxxxxx', 'xxxxxx@gmail.com', '$2y$10$/vnBgX4SId/.m1hhzZlvFO9M6EbSRGnYqq3q7YrHdQ0iFZv2j1fam', '1608124393.png', 'ACTIVE', '2020-12-16 13:13:14', '2020-12-16 13:13:14'),
('7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Komang Ayu Oka Maylia Widyanti SBG', 'didi.ariyana16@gmail.com', '$2y$10$PtFHtzocg00Cb51WmRPRP./TcGYzhXjzSX70zSXIT3cGwtNsc3LGm', '1603435726.png', 'ACTIVE', '2020-10-23 06:48:47', '2020-10-23 06:57:55'),
('84e86dda-6e64-44be-ba68-241d0b02b44b', 'suryawid', 'suryawidyana@gmail', '$2y$10$PtFHtzocg00Cb51WmRPRP./TcGYzhXjzSX70zSXIT3cGwtNsc3LGm', NULL, 'ACTIVE', '2020-10-23 07:10:20', '2020-10-23 07:10:20'),
('d80749a4-3a6d-45c4-b57b-e4a6356771ad', 'I Wayan didik', 'user@gmail.com', '$2y$10$PtFHtzocg00Cb51WmRPRP./TcGYzhXjzSX70zSXIT3cGwtNsc3LGm', '1605365067.png', 'ACTIVE', '2020-11-14 14:44:27', '2020-11-14 14:44:27'),
('f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'didik', 'didikariyana@gmail.com', '$2y$10$Y4shtDtQzhmDtBWiX7I8EeJK41VDoF0Q8esDh92gu9dKN5p2OlUA2', '1605101448.png', 'ACTIVE', '2020-11-11 13:30:49', '2020-11-11 13:34:17');

-- --------------------------------------------------------

--
-- Table structure for table `partner_details`
--

CREATE TABLE `partner_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `is_marriage` tinyint(1) DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_education` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_activity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ijazah` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partner_details`
--

INSERT INTO `partner_details` (`id`, `partner_id`, `address`, `birth_place`, `birth_date`, `is_marriage`, `phone_number`, `phone_type`, `last_education`, `last_activity`, `ktp`, `ijazah`, `kk`, `created_at`, `updated_at`) VALUES
('00073e37-2f1f-43d5-94dd-7e5b804c1731', 'd80749a4-3a6d-45c4-b57b-e4a6356771ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-14 14:44:27', '2020-11-14 14:44:27'),
('31325ab9-c2e9-4a0c-8695-ff2bf4bf1e3d', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Jln Rambutan no 33 Bangli', 'Bangli', '1999-05-20', 0, '081237242562', 'Samsung A30', 'SMA', 'tidak ada', '1603436275.jpeg', '1603436275.jpeg', '1603436275.jpg', '2020-10-23 06:48:47', '2020-10-23 06:57:55'),
('560ef170-9943-48c4-9517-b4329d88e9ba', '434252bf-1814-481a-a6a4-e521cfa7f4d4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-16 13:13:14', '2020-12-16 13:13:14'),
('a7751f81-1e55-42dc-9220-7c98f85c89a6', 'f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'jalan', 'denpasar', '2007-06-11', 1, '1203120312030120', 'samsung s10', 'SMA', 'sensus', '1605101657.jpeg', '1605101657.jpeg', '1605101657.jpg', '2020-11-11 13:30:49', '2020-11-11 13:34:17'),
('bf6d7c50-b64f-4690-837a-44a222460597', '84e86dda-6e64-44be-ba68-241d0b02b44b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-23 07:10:20', '2020-10-23 07:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
('52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 'Entrie Data', '2020-10-23 06:50:24', '2020-10-23 06:50:24'),
('7afa2d13-4a67-4fc0-b525-9e05336ebfc0', 'Pencacah', '2020-10-23 06:50:55', '2020-10-23 06:50:55'),
('c3875374-722f-4491-946c-e8b806085900', 'Pengawas', '2020-10-23 06:51:08', '2020-10-23 06:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci,
  `description_long` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `description_short`, `description_long`, `photo`, `type`, `created_at`, `updated_at`) VALUES
('71454693-3104-4355-ad0a-22d6e3a8657c', 'Profil Badan Pusat Statistik Kabupaten Bangli', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', '/home/images/bps_logo.svg', 'ABOUT_PAGE', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE `vacancies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_period` datetime NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`id`, `administrator_id`, `photo`, `title`, `description`, `end_period`, `status`, `created_at`, `updated_at`) VALUES
('cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', '1603435923.jpg', 'Sensus Penduduk 2020', '<p>SENSUS PENDUDUK</p>', '2020-12-31 00:00:00', 'ACTIVE', '2020-10-23 06:52:03', '2020-11-21 00:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_applies`
--

CREATE TABLE `vacancy_applies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vacancy_applies`
--

INSERT INTO `vacancy_applies` (`id`, `vacancy_id`, `administrator_id`, `position_id`, `partner_id`, `status`, `created_at`, `updated_at`) VALUES
('2090f37e-79a0-4582-9d59-7262e82c4d6f', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '7afa2d13-4a67-4fc0-b525-9e05336ebfc0', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'UNAPPROVED', '2020-12-07 03:18:51', '2020-12-07 03:18:51'),
('4bca8a76-c5e9-4445-afde-946ed92ed8ea', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'REJECTED', '2020-11-21 00:39:40', '2020-12-02 11:34:16'),
('bc64c295-e810-4e06-9781-75af2e105a38', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 'f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'REJECTED', '2020-11-19 10:02:56', '2020-11-19 11:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_details`
--

CREATE TABLE `vacancy_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_partner` int(11) DEFAULT NULL,
  `current_partner` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vacancy_details`
--

INSERT INTO `vacancy_details` (`id`, `vacancy_id`, `position_id`, `max_partner`, `current_partner`, `created_at`, `updated_at`) VALUES
('0cc53407-6740-42c8-b244-dba089e7af22', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', 'c3875374-722f-4491-946c-e8b806085900', 1, 0, '2020-11-21 00:01:33', '2020-11-21 00:01:33'),
('96cd989b-54ed-4efe-994b-34d867cd61ce', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '7afa2d13-4a67-4fc0-b525-9e05336ebfc0', 1, 0, '2020-11-20 23:54:11', '2020-11-21 00:01:33'),
('cc63075c-1d1c-4182-8af8-91ae21d24ea5', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 3, 2, '2020-10-23 06:52:03', '2020-12-02 11:38:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `administrators_email_unique` (`email`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interviews`
--
ALTER TABLE `interviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `partners_email_unique` (`email`);

--
-- Indexes for table `partner_details`
--
ALTER TABLE `partner_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancy_applies`
--
ALTER TABLE `vacancy_applies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancy_details`
--
ALTER TABLE `vacancy_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
