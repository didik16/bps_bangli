<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('administrators')->delete();

        \DB::table('administrators')->insert(array(
            0 => array(
                'id' => \Uuid::generate(4),
                'name' => 'Admin Master',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('password'),
                'photo' => NULL
        )
            ));
    }
}
