<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::group(['middleware' => 'auth.administrators'], function () {
        Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard.index');
        Route::resource('/user/administrator', 'Admin\AdminController');
        Route::resource('/user/partner', 'Admin\PartnerController');
        // Route::post('/user/partner/{id}/destroy', 'Admin\PartnerController@destroy')->name('destroy_partner');

        Route::resource('/vacancy', 'Admin\VacancyController');

        Route::get('/vacancy/{id}/applies', 'Admin\VacancyController@applies')->name('vacancy.applies.index');
        Route::get('/vacancy/{id}/applies/{apply_id}', 'Admin\VacancyController@applies_action')->name('vacancy.applies.action');

        Route::get('/vacancy/{id}/interview', 'Admin\InterviewController@interview')->name('interview');
        Route::post('/store_interview', 'Admin\InterviewController@store_interview')->name('store_interview');

        Route::get('/vacancy/{id}/submit_interview', 'Admin\InterviewController@submit_interview')->name('submit_interview');
        Route::post('/store_submit_interview', 'Admin\InterviewController@store_submit_interview')->name('store_submit_interview');

        Route::resource('/announcement', 'Admin\AnnouncementController');

        Route::resource('/activity', 'Admin\ActivityController');


        Route::resource('/setting', 'Admin\SettingController');
        Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
            Route::get('/about_us/edit/{id}', 'Admin\SettingController@edit_about_us')->name('edit_about_us');
            Route::post('/about_us/update_about_us/{id}', 'Admin\SettingController@update_about_us')->name('update_about_us');
            Route::get('/about_us/add_about_us', 'Admin\SettingController@create_about_us')->name('create_about_us');
            Route::post('/about_us/store_about_us', 'Admin\SettingController@store_about_us')->name('store_about_us');
        });

        Route::resource('/position', 'Admin\PositionController');
        Route::get('/position/detail/{id}', 'Admin\PositionController@detail_posisi')->name('detail_position');
    });

    Auth::routes();
    Route::get('/logout', 'Auth\LoginController@logout');
});


Route::get('/pusher', function () {
    return view('welcome2');
});

Route::get('test', function () {
    event(new App\Events\StatusLiked('Someone'));
    return "Event has been sent!";
});

// Login

Route::get('/login', 'Auth\LoginController@login_partner')->name('login.index');
Route::get('/register', 'Auth\RegisterController@register_partner')->name('register.index');
Route::post('/register', 'Auth\RegisterController@register_partner_store')->name('register.store');
Route::get('/logout', 'Auth\LoginController@logout_partner')->name('logout');

Route::post('/upload', 'UploadController@upload')->name('upload');

Route::group(['as' => 'home.'], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('/about', 'HomeController@about')->name('about');
    Route::get('/vacancy', 'HomeController@vacancy')->name('vacancy');
    Route::get('/vacancy/{id}', 'HomeController@vacancy_detail')->name('vacancy.detail');
    Route::post('/vacancy/{id}/apply', 'HomeController@vacancy_apply')->name('vacancy.apply');
    Route::get('/announcement', 'HomeController@announcement')->name('announcement');
    Route::get('/announcement/{id}', 'HomeController@announcement_detail')->name('announcement.detail');
    Route::get('/activity', 'HomeController@activity')->name('activity');

    //profile

    Route::get('/account/profile', 'HomeController@profile')->name('profile');
    Route::post('/account/profile', 'HomeController@profile_update')->name('profile.update');

    Route::get('/account/applies', 'HomeController@applies')->name('applies');
    Route::get('/account/applies/{id}/detail', 'HomeController@applies_detail')->name('applies_detail');
    Route::get('/account/applies/{id}/cancel', 'HomeController@applies_cancel')->name('applies.cancel');
});

// Route::get('test_admin', function () {
//     event(new App\Events\NotifAdmin('Lolosneee', 'Didik Ariyana', '1605365067.png', '2020-12-02 19:34:16'));
//     return "Event has been sent!";
// });
