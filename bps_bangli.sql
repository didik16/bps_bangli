-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Nov 2020 pada 15.27
-- Versi server: 10.1.33-MariaDB
-- Versi PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bps_bangli`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `activities`
--

CREATE TABLE `activities` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `activities`
--

INSERT INTO `activities` (`id`, `administrator_id`, `title`, `description`, `photo`, `status`, `created_at`, `updated_at`) VALUES
('e9f44863-293d-42bf-b2bd-f2d580f3f5b9', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'Sensus Penduduk 2020', 'wadwawawd', 'http://127.0.0.1:8000/uploads/activity/1603436009.jpeg', 'PUBLISHED', '2020-10-23 06:53:29', '2020-10-23 06:53:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `administrators`
--

CREATE TABLE `administrators` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `administrators`
--

INSERT INTO `administrators` (`id`, `name`, `email`, `password`, `photo`, `created_at`, `updated_at`) VALUES
('4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'Admin Master', 'admin@gmail.com', '$2y$10$hEDNJrCVln4tWUxME0/HC.QtFRf7M09M3fM2H9QnctwOwbadHhZ.2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcements`
--

CREATE TABLE `announcements` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `announcements`
--

INSERT INTO `announcements` (`id`, `administrator_id`, `title`, `description`, `status`, `photo`, `created_at`, `updated_at`) VALUES
('c06a1919-16f6-46de-81f3-40d0b00db2d3', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'sakernas', '<p>aaasda</p>', 'PUBLISHED', 'http://127.0.0.1:8000/uploads/announcement/1603437769.png', '2020-10-23 07:22:49', '2020-10-23 07:22:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(47, '2014_10_12_000000_create_users_table', 1),
(48, '2014_10_12_100000_create_password_resets_table', 1),
(49, '2020_10_12_070435_create_administrators_table', 1),
(50, '2020_10_12_183418_create_partners_table', 1),
(51, '2020_10_13_091515_create_vanancies_table', 1),
(52, '2020_10_13_140119_create_announcements_table', 1),
(53, '2020_10_13_142213_create_galleries_table', 1),
(54, '2020_10_13_144235_create_partner_details_table', 1),
(55, '2020_10_14_114032_create_settings_table', 1),
(56, '2020_10_14_132045_create_positions_table', 1),
(57, '2020_10_14_135736_add_column_description_on_galleries_table', 2),
(58, '2020_10_14_141108_create_activities_table', 2),
(59, '2020_10_14_170523_create_vacancy_details_table', 3),
(60, '2020_10_14_171824_delete_column_max_participant', 3),
(61, '2020_10_21_203339_create_vacancy_applies_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners`
--

CREATE TABLE `partners` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `partners`
--

INSERT INTO `partners` (`id`, `name`, `email`, `password`, `photo`, `status`, `created_at`, `updated_at`) VALUES
('7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Komang Ayu Oka Maylia Widyanti SBG', 'widyantisbg@gmail.com', '$2y$10$qpkx0.7JhApsvvas8jFnFOYQeX8L1UVJeDkUmFYZOp5KgW4Cn3xXy', 'http://127.0.0.1:8000/uploads/avatar/partner/1603435726.png', 'ACTIVE', '2020-10-23 06:48:47', '2020-10-23 06:57:55'),
('84e86dda-6e64-44be-ba68-241d0b02b44b', 'suryawid', 'suryawidyana@gmail', '$2y$10$APPFfgRat0iUPJzT8oMq5ezBFfFl28fJiw2n/1zGv9YmtGWslsn6q', NULL, 'ACTIVE', '2020-10-23 07:10:20', '2020-10-23 07:10:20'),
('f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'didik', 'didikariyana@gmail.com', '$2y$10$Y4shtDtQzhmDtBWiX7I8EeJK41VDoF0Q8esDh92gu9dKN5p2OlUA2', 'http://127.0.0.1:8000/uploads/avatar/partner/1605101448.png', 'ACTIVE', '2020-11-11 13:30:49', '2020-11-11 13:34:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partner_details`
--

CREATE TABLE `partner_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `is_marriage` tinyint(1) DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `last_education` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_activity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ijazah` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `partner_details`
--

INSERT INTO `partner_details` (`id`, `partner_id`, `address`, `birth_place`, `birth_date`, `is_marriage`, `phone_number`, `phone_type`, `age`, `last_education`, `last_activity`, `ktp`, `ijazah`, `kk`, `created_at`, `updated_at`) VALUES
('31325ab9-c2e9-4a0c-8695-ff2bf4bf1e3d', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'Jln Rambutan no 33 Bangli', 'Bangli', '1999-05-20', 0, '081237242562', 'Samsung A30', 21, 'SMA', 'tidak ada', 'http://127.0.0.1:8000/uploads/partner/files/1603436275.jpeg', 'http://127.0.0.1:8000/uploads/partner/files/1603436275.jpeg', 'http://127.0.0.1:8000/uploads/partner/files/1603436275.jpg', '2020-10-23 06:48:47', '2020-10-23 06:57:55'),
('a7751f81-1e55-42dc-9220-7c98f85c89a6', 'f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'jalan', 'denpasar', '2007-06-11', 1, '1203120312030120', 'samsung s10', 20, 'SMA', 'sensus', 'http://127.0.0.1:8000/uploads/partner/files/1605101657.jpeg', 'http://127.0.0.1:8000/uploads/partner/files/1605101657.jpeg', 'http://127.0.0.1:8000/uploads/partner/files/1605101657.jpg', '2020-11-11 13:30:49', '2020-11-11 13:34:17'),
('bf6d7c50-b64f-4690-837a-44a222460597', '84e86dda-6e64-44be-ba68-241d0b02b44b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-23 07:10:20', '2020-10-23 07:10:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `positions`
--

CREATE TABLE `positions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
('52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 'Entrie Data', '2020-10-23 06:50:24', '2020-10-23 06:50:24'),
('7afa2d13-4a67-4fc0-b525-9e05336ebfc0', 'Pencacah', '2020-10-23 06:50:55', '2020-10-23 06:50:55'),
('c3875374-722f-4491-946c-e8b806085900', 'Pengawas', '2020-10-23 06:51:08', '2020-10-23 06:51:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci,
  `description_long` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `title`, `description_short`, `description_long`, `photo`, `type`, `created_at`, `updated_at`) VALUES
('71454693-3104-4355-ad0a-22d6e3a8657c', 'Profil Badan Pusat Statistik Kabupaten Bangli', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'https://upload.wikimedia.org/wikipedia/commons/2/28/Lambang_Badan_Pusat_Statistik_%28BPS%29_Indonesia.svg', 'ABOUT_PAGE', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `vacancies`
--

CREATE TABLE `vacancies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_period` datetime NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `vacancies`
--

INSERT INTO `vacancies` (`id`, `administrator_id`, `photo`, `title`, `description`, `end_period`, `status`, `created_at`, `updated_at`) VALUES
('cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '4d8d0a3c-e0ae-45ef-915b-f8c3cf636049', 'http://127.0.0.1:8000/uploads/vacancy/1603435923.jpg', 'Sensus Penduduk 2020', '<p>SENSUS PENDUDUK aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>', '2020-10-25 00:00:00', 'ACTIVE', '2020-10-23 06:52:03', '2020-10-23 06:59:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vacancy_applies`
--

CREATE TABLE `vacancy_applies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `vacancy_applies`
--

INSERT INTO `vacancy_applies` (`id`, `vacancy_id`, `administrator_id`, `position_id`, `partner_id`, `status`, `created_at`, `updated_at`) VALUES
('491cac8f-90ba-401e-9811-2e60296b29f1', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'REJECTED', '2020-10-23 07:00:07', '2020-10-23 07:01:11'),
('4e09ca51-e736-46d5-9747-2ba06786d1b5', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 'f844d9d2-726b-41e4-8e9b-2ebfdb4b03fc', 'APPROVED', '2020-11-11 13:37:30', '2020-11-11 13:38:54'),
('5dc50034-921e-4a94-b214-c29796389bc5', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', NULL, '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', '7b702f41-58d0-43ef-9639-3c47bf4a5b97', 'UNAPPROVED', '2020-10-23 07:03:36', '2020-10-23 07:03:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vacancy_details`
--

CREATE TABLE `vacancy_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_partner` int(11) DEFAULT NULL,
  `current_partner` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `vacancy_details`
--

INSERT INTO `vacancy_details` (`id`, `vacancy_id`, `position_id`, `max_partner`, `current_partner`, `created_at`, `updated_at`) VALUES
('cc63075c-1d1c-4182-8af8-91ae21d24ea5', 'cd09ca17-f357-4d6f-80fd-ae2b03586ef2', '52671b0a-4e1b-4eff-ad36-56da2a45c5d7', 3, 0, '2020-10-23 06:52:03', '2020-11-11 13:45:22');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `administrators_email_unique` (`email`);

--
-- Indeks untuk tabel `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `partners_email_unique` (`email`);

--
-- Indeks untuk tabel `partner_details`
--
ALTER TABLE `partner_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `vacancy_applies`
--
ALTER TABLE `vacancy_applies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `vacancy_details`
--
ALTER TABLE `vacancy_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
