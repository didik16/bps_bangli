<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'id',
        'title',
        'description_short',
        'description_long',
        'photo',
        'type'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }
}
