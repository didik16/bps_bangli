<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Announcement extends Model
{
    protected $table = 'announcements';

    protected $fillable = [
        'administrator_id',
        'photo',
        'title',
        'description',
        'status' //published, unpublished
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function administrator()
    {
        return $this->belongsTo('App\Administrator');
    }
}
