<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Position extends Model
{
    protected $table = 'positions';

    protected $fillable = [
        'name',
        'deskripsi'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }
}
