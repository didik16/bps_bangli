<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateStatusUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crontupdatestatus_user:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = DB::table('partners')
            ->where('status', 'ACTIVE')
            ->whereRaw('date_add(created_at, interval 1 montoh) < now()')
            ->get();

        foreach ($user as $user) {
            $update = DB::table('partners')
                ->where('status', 'ACTIVE')
                ->update([
                    'status' => 'INACTIVE',
                ]);

            echo "Update Mitra $update ";
        }
    }
}
