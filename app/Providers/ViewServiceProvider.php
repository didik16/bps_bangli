<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Notification;
use App\Partner;

use Auth;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // if(Auth::guard('partners')->check()){
            View::composer(
                'components.header', 'App\Http\ViewComposer\ProfileComposer'
            );
        // }

        view::composer('admin.components.header', function ($notif){

            $admin = \Auth::guard('administrators')->user()->id;

            $all_notif = Notification::where('pesan', 'like', '%' .'melamar' . '%')
                                    ->join('partners', 'notifications.partner_id', '=', 'partners.id')
                                    ->orderBy('notifications.created_at','DESC')
                                    ->limit(5)
                                    ->get();

            $notif->with(['notif' => $all_notif]);
        });


    }
}
