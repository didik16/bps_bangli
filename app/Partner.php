<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Uuid;

class Partner extends Authenticatable
{
    use Notifiable;

    protected $table = 'partners';

    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
        'status'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function detail()
    {
        return $this->hasOne('App\PartnerDetail');
    }

    public function vacancy_applies()
    {
        return $this->hasMany('App\VacancyApply');
    }
}
