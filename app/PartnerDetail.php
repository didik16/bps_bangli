<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class PartnerDetail extends Model
{
    protected $table = 'partner_details';

    protected $appends = ['completeness'];

    protected $fillable = [
        'partner_id',
        'address',
        'birth_place',
        'birth_date',
        'is_marriage',
        'phone_number',
        'phone_type',
        'last_education',
        'last_activity',
        'ktp',
        'ijazah',
        'kk'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function getCompletenessAttribute()
    {

        $listCompleteness = [
            'address',
            'birth_place',
            'birth_date',
            'is_marriage',
            'phone_number',
            'is_marriage',
            'phone_number',
            'phone_type',
            'last_education',
            'last_activity',
            'ktp',
            'ijazah',
            'kk'
        ];

        $completeCount = 0;

        for ($i = 0; $i < count($listCompleteness); $i++) {
            if ($listCompleteness[$i] == 'is_marriage' && !is_null($this[$listCompleteness[$i]])) {
                $completeCount++;
                continue;
            }
            if (!is_null($this[$listCompleteness[$i]]) && !empty($this[$listCompleteness[$i]])) {
                $completeCount++;
            }
        }

        $percentage = $completeCount / count($listCompleteness) * 100;

        return round($percentage);
    }
}
