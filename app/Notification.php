<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'pesan',
        'status',
        'partner_id',
        // UNAPPROVED, APPROVED, REJECTED
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }
}
