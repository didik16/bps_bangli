<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class VacancyApply extends Model
{
    protected $table = 'vacancy_applies';

    protected $fillable = [
        'position_id',
        'vacancy_id',
        'administrator_id',
        'partner_id',
        'status' // UNAPPROVED, APPROVED, REJECTED
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    public function vacancy()
    {
        return $this->belongsTo('App\Vacancy');
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
}
