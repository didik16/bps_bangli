<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Interview extends Model
{
    protected $table = 'interviews';

    protected $fillable = [
        'vacancy_applies_id',
        'position_id',
        'vacancy_id',
        'tanggal',
        'nilai',
        'partner_id',
        'pewawancara',
        // UNAPPROVED, APPROVED, REJECTED
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function vacancy()
    {
        return $this->belongsTo('App\Vacancy');
    }
}
