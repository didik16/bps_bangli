<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class VacancyDetail extends Model
{
    protected $table = 'vacancy_details';

    protected $fillable = [
        'position_id',
        'vacancy_id',
        'max_partner',
        'current_partner'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function position()
    {
        return $this->belongsTo('App\Position');
    }
}
