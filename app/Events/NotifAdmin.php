<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotifAdmin implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $foto;

    public $message;
    public $waktu;
    public $nama;

    /**
     * Create a new event instance.
     *
    * @return void
     */
    public function __construct($message, $nama, $foto, $waktu)
    {
        // $this->username = $username;
        $this->message  = $message; 
        $this->foto  = $foto; 
        $this->waktu  = $waktu; 
        $this->nama  = $nama; 
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['notif-admin'];
    }
}