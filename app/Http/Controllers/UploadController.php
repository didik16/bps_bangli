<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function upload(Request $request){
        if($request->hasFile('upload')){
            $folder = "{$request->path}/";

            $file = $request->file('upload');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move("uploads/$folder", $file_name);
                $req['photo'] = url("uploads/" . $folder . $file_name);

                return response()->json([
                    'filename' => 'upload',
                    'uploaded' => 1,
                    'url' => $req['photo']
                ]);
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }
    }
}
