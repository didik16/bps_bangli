<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Setting;
use App\AboutUs;
use Auth;

use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = Setting::where('type', 'ABOUT_PAGE')->first();
        $about_us = AboutUs::orderBy('created_at', 'DESC')->get();

        return view('admin.setting.edit', [
            'about' => $about,
            'about_us' => $about_us
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->_type == "ABOUT_PAGE") {

            $data = Setting::where('id', $id)->where('type', 'ABOUT_PAGE')->firstOrFail();

            $request->validate([
                'title' => 'required',
                'description_short' => 'required',
                'description_long' => 'required',
                'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            ]);

            $req = [
                'title' => $request->title,
                'description_short' => $request->description_short,
                'description_long' => $request->description_long,
            ];

            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $file_extension = $file->getClientOriginalExtension();
                $file_name = time() . "." . $file_extension;

                try {
                    $file->move('uploads/setting/about-us/', $file_name);
                    $req['photo'] = url("uploads/setting/about-us/$file_name");
                } catch (\Exception $e) {
                    return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
                }
            }

            DB::beginTransaction();
            try {
                $data->update($req);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }

            return redirect()
                ->back()
                ->with('success', 'Data Successfully Updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function edit_about_us($id)
    {
        $data = AboutUs::findOrFail($id);
        return view('admin.setting.about_us.edit', [
            'data' => $data
        ]);
    }

    public function update_about_us(Request $request, $id)
    {
        $data = AboutUs::findOrFail($id);

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);

        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];

        if ($request->status) {
            $req['status'] = "PUBLISHED";
        } else {
            $req['status'] = "UNPUBLISHED";
        }

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/about_us/', $file_name);
                // $req['photo'] = url("uploads/activity/$file_name");
                $req['photo'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try {
            $data->update($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Updated!');
    }


    public function create_about_us()
    {
        return view('admin.setting.about_us.create');
    }

    public function store_about_us(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);

        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];

        if ($request->status) {
            $req['status'] = "PUBLISHED";
        } else {
            $req['status'] = "UNPUBLISHED";
        }

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/about_us/', $file_name);
                // $req['photo'] = url("uploads/activity/$file_name");
                $req['photo'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try {
            AboutUs::create($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Added!');
    }
}
