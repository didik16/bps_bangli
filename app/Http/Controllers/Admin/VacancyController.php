<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Vacancy;
use Auth;
use App\Position;
use App\VacancyDetail;
use App\VacancyApply;
use App\Partner;
use App\Interview;
use Mail; 

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Vacancy::orderBy('created_at','DESC')->get();

        return view('admin.vacancy.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Position::orderBy('name', 'ASC')->get();

        return view('admin.vacancy.create',[
            'positions' => $positions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'end_period' => 'required|date',
            'photo' => 'required|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean',
        ]);

        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'end_period' => \Carbon\Carbon::parse($request->end_period),
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];

        if($request->status){
            $req['status'] = "ACTIVE";
        }
        else{
            $req['status'] = "INACTIVE";
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/vacancy/', $file_name);
                // $req['photo'] = url("uploads/vacancy/$file_name");
                $req['photo'] = $file_name;
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            $data = Vacancy::create($req);
            foreach($request->positions as $position){
                VacancyDetail::create([
                    'vacancy_id' => $data->id,
                    'position_id' => $position['id'],
                    'max_partner' => $position['max_partner'],
                    'current_partner' => 0
                ]);
            }
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Vacancy::with(['detail', 'detail.position'])->where('id',$id)->firstOrFail();
        $positions = Position::orderBy('name', 'ASC')->get();

        return view('admin.vacancy.edit', [
            'data' => $data,
            'positions' => $positions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Vacancy::where('id', $id)->firstOrFail();

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'end_period' => 'required|date',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);

        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'end_period' => \Carbon\Carbon::parse($request->end_period),
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];


        if($request->status){
            $req['status'] = "ACTIVE";
        }
        else{
            $req['status'] = "INACTIVE";
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/vacancy/', $file_name);
                // $req['photo'] = url("uploads/vacancy/$file_name");
                $req['photo'] = $file_name;
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }



        DB::beginTransaction();
        try{
            $data->update($req);
            foreach($request->positions as $position){

                // $cek = VacancyDetail::where('vacancy_id', '=', $data->id)
                // ->where('position_id', '=', $position['id'])->get();


                if(VacancyDetail::where('vacancy_id', $data->id)->where('position_id', $position['id'])->exists()){
                   
                    VacancyDetail::where('vacancy_id', $data->id)->where('position_id', $position['id'])->update([
                        'max_partner' => $position['max_partner']
                    ]);

                }else{
                    VacancyDetail::create([
                        'vacancy_id' => $data->id,
                        'position_id' => $position['id'],
                        'max_partner' => $position['max_partner'],
                        'current_partner' => 0
                    ]);
                }

            }
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Vacancy::where('id', $id)->first();

        if(!$data){
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ]);
        }

        DB::beginTransaction();
        try{

            $data->delete();

            DB::commit();
        }
        catch(\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data successfully deleted'
        ]);
    }

    public function applies(Request $request, $id){
        $data = Vacancy::with(['detail','detail.position', 'applies.position', 'applies.partner'])->where('id', $id)->firstOrFail();

        return view('admin.vacancy.applies.index')->with([
            'data' => $data
        ]);
    }

    public function applies_action(Request $request, $id, $apply_id){
        $data = VacancyApply::where('vacancy_id', $id)->where('id', $apply_id)->firstOrFail();
        $user = Partner::where('id', $data->partner_id)->firstOrFail();

        $penirima=$user->email;

        if($request->action == "approved"){
            $data->update([
                'status' => 'APPROVED'
            ]);

            //Kirim email notifikasi
            // try{
            //     Mail::send('email', ['nama' => $user->name, 'pesan' => 'Selamat anda Lulus tahap Admininstrasi'], function ($message) use ($penirima)
            //     {
            //         $message->subject('Informasi Penerimaan Lowongan - Status Administrasi');
            //         $message->from('info@riskyarya.com', 'BPS Bangli');
            //         $message->to($penirima);
            //     });
                
            //     }
            // catch (Exception $e){
            //     return response (['status' => false,'errors' => $e->getMessage()]);
            // }

             return redirect('admin/vacancy/'.$data->id.'/interview');
        }


        if($request->action == "approved-interview"){
            $data->update([
                'status' => 'APPROVED'
            ]);

            //Kirim email notifikasi
            // try{
            //     Mail::send('email', ['nama' => $user->name, 'pesan' => 'Selamat anda Lulus dalam Lowongan XXXX'], function ($message) use ($penirima)
            //     {
            //         $message->subject('Informasi Penerimaan Lowongan - Status Interview');
            //         $message->from('info@riskyarya.com', 'BPS Bangli');
            //         $message->to($penirima);
            //     });
                
            //     }
            // catch (Exception $e){
            //     return response (['status' => false,'errors' => $e->getMessage()]);
            // }

            $vacancy_detail = VacancyDetail::where('vacancy_id', $id)->where('position_id', $data->position_id)->firstOrFail();
            $position = Position::where('id', $data->position_id)->firstOrFail();

            $current_partner = $vacancy_detail->current_partner + 1;

            if($vacancy_detail->max_partner < $current_partner){
                return redirect()->back()->with([
                    'error' => 'Sorry, quota position of ' . $position->name . ' for this vacancy is full'
                ]);
            }

            $vacancy_detail->update([
                'current_partner' => $current_partner
            ]);

            return redirect()->back()->with([
                'success' => 'Data successfully updated'
            ]);
        }

        if($request->action == "rejected"){

            $vacancy_detail = VacancyDetail::where('vacancy_id', $id)->where('position_id', $data->position_id)->firstOrFail();
            $position = Position::where('id', $data->position_id)->firstOrFail();


            $current_partner = $vacancy_detail->current_partner - 1;

            if($current_partner<=0){
                $current_partner=0;
            }

            if($data->status != "UNAPPROVED"){
                $vacancy_detail->update([
                    'current_partner' => $current_partner
                ]);
            }

            $data->update([
                'status' => 'REJECTED'
            ]);

            return redirect()->back()->with([
                'success' => 'Data successfully updated'
            ]);
        }
    }

    


}
