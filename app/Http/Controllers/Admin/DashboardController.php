<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Announcement;
use App\Activity;
use App\Administrator;
use App\Position;
use App\Vacancy;
use App\Partner;

class DashboardController extends Controller
{
    public function index(Request $request){
        $announcement_count = Announcement::count();
        $activity_count = Activity::count();
        $administrator_count = Administrator::count();
        $position_count = Position::count();
        $vacancy_count = Vacancy::count();
        $partner_count = Partner::count();

        return view('admin.dashboard', [
            'announcement_count' => $announcement_count,
            'activity_count' => $activity_count,
            'administrator_count' => $administrator_count,
            'position_count' => $position_count,
            'vacancy_count' => $vacancy_count,
            'partner_count' => $partner_count
        ]);
    }
}
