<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Position;
use App\Partner;
use App\VacancyApply;
use App\Vacancy;
use Illuminate\Support\Facades\DB;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Position::orderBy('created_at', 'DESC')->get();

        return view('admin.position.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'status' => 'nullable|boolean'
        ]);
        $req = [
            'name' => $request->name,
            'deskripsi' => $request->description,
        ];

        if ($request->status) {
            $req['status'] = "PUBLISHED";
        } else {
            $req['status'] = "UNPUBLISHED";
        }

        DB::beginTransaction();
        try {
            Position::create($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Position::findOrFail($id);
        return view('admin.position.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Position::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'status' => 'nullable|boolean'
        ]);
        $req = [
            'name' => $request->name,
            'deskripsi' => $request->description,
        ];

        if ($request->status) {
            $req['status'] = "ACTIVE";
        } else {
            $req['status'] = "INACTIVE";
        }

        DB::beginTransaction();
        try {
            $data->update($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Position::where('id', $id)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ]);
        }

        DB::beginTransaction();
        try {

            $data->delete();

            DB::commit();
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data successfully deleted'
        ]);
    }



    public function detail_posisi($id)
    {
        $VacancyApplys = VacancyApply::where('position_id', $id)->where('status', 'APPROVED')->get();
        foreach ($VacancyApplys as $VacancyApply) {
        }

        if (count($VacancyApplys) > 0) {
            $Partner = Partner::where('id', $VacancyApply->partner_id)->get();
            $vacancy = Vacancy::where('id', $VacancyApply->vacancy_id)->get();
            $position = Position::where('id', $VacancyApply->position_id)->firstOrFail();

            return view('admin.position.detail')->with([
                'partner' => $Partner,
                'vacancy' => $vacancy,
                'position' => $position,
                'vacancyApply' => $VacancyApply
            ]);
        }
        return view('admin.position.detail');
    }
}
