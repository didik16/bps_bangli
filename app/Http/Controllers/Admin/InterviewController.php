<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Vacancy;
use Auth;
use App\Position;
use App\VacancyDetail;
use App\VacancyApply;
use App\Partner;
use App\PartnerDetail;
use App\Interview;
use App\Notification;
use Mail;
use App\Events\StatusLiked;


class InterviewController extends Controller
{

    public function interview($id)
    {
        $data = VacancyApply::with(['partner'])->where('id', $id)->firstOrFail();
        $partner = Partner::where('id', $data->partner_id)->firstOrFail();

        $vacancy = Vacancy::where('id', $data->vacancy_id)->firstOrFail();
        $position = Position::where('id', $data->position_id)->firstOrFail();



        return view('admin.vacancy.interview', [
            'data' => $data,
            'partner' => $partner,
            'vacancy' => $vacancy,
            'position' => $position

        ]);
    }

    public function submit_interview($id)
    {
        $data = VacancyApply::with(['partner'])->where('id', $id)->firstOrFail();
        $partner = Partner::where('id', $data->partner_id)->firstOrFail();

        $vacancy = Vacancy::where('id', $data->vacancy_id)->firstOrFail();
        $position = Position::where('id', $data->position_id)->firstOrFail();
        $interview = Interview::where('vacancy_applies_id', $id)->firstOrFail();



        return view('admin.vacancy.submit-interview', [
            'data' => $data,
            'partner' => $partner,
            'vacancy' => $vacancy,
            'position' => $position,
            'interview' => $interview,
        ]);
    }


    public function store_interview(Request $req)
    {

        $data = VacancyApply::with(['partner'])->where('id', $req->applies_id)->firstOrFail();
        $user = Partner::where('id', $data->partner_id)->firstOrFail();


        $penirima = $user->email;

        $messages = [
            'tanggal.required' => 'Mohon isi form Tanggal',
            'pewawancara.required' => 'Mohon isi form Pewawancara',
        ];

        $this->validate($req, [
            'tanggal' => 'required',
            'pewawancara' => 'required',
        ], $messages);


        Interview::create([
            'vacancy_applies_id' => $data->id,
            'vacancy_id' =>  $data->vacancy_id,
            'position_id' => $data->position_id,
            'partner_id' => $data->partner_id,
            'tanggal' => $req->tanggal,
            'pewawancara' => $req->pewawancara
        ]);

        $data->update([
            'status' => 'INTERVIEW'
        ]);



        $pesan_notif = 'Selamat anda lolos tahap Administrasi';

        Notification::create([
            'partner_id' => $data->partner_id,
            'pesan' =>  $pesan_notif,
        ]);

        //kirim live notif
        event(new StatusLiked($pesan_notif, $user->id));

        $vacancy = Vacancy::where('id', $data->vacancy_id)->firstOrFail();

        $pesan_email = '<p>Selamat anda Lulus tahap Administrasi dalam Lowongan ' . $vacancy->title . ', tahap berikutnya adalah Interview</p>
        <p><strong>Tanggal : ' . $req->tanggal . '</strong></p>          
        <p><strong>Lokasi  : Kantor BPS Bangli</strong></p>
        <p><strong>Pakaian : Bebas Rapi</strong></p>';

        //Kirim email notifikasi
        //  try{
        //     Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan_email], function ($message) use ($penirima)
        //     {
        //         $message->subject('Informasi Penerimaan Lowongan - Status Administrasi');
        //         $message->from('info@riskyarya.com', 'BPS Bangli');
        //         $message->to($penirima);
        //     });

        //     }
        // catch (Exception $e){
        //     return response (['status' => false,'errors' => $e->getMessage()]);
        // }

        return redirect('admin/vacancy/' . $data->vacancy_id . '/applies')->with([
            'success' => 'Data successfully updated'
        ]);
        // return redirect()->back()->with([
        //     'success' => 'Data successfully updated'
        // ]);

    }


    public function store_submit_interview(Request $req)
    {

        $data = VacancyApply::with(['partner'])->where('id', $req->applies_id)->firstOrFail();
        $user = Partner::where('id', $data->partner_id)->firstOrFail();
        $user_detail = PartnerDetail::where('partner_id', $data->partner_id)->firstOrFail();
        $interview = Interview::where('vacancy_applies_id', $req->applies_id)->firstOrFail();
        $position = Position::where('id', $data->position_id)->firstOrFail();
        $vacancy = Vacancy::where('id', $data->vacancy_id)->firstOrFail();
        $vacancy_detail = VacancyDetail::where('vacancy_id', $data->vacancy_id)->where('position_id', $data->position_id)->firstOrFail();



        $penirima = $user->email;

        $messages = [
            'nilai.required' => 'Mohon isi form Nilai',
            'nilai.numeric' => 'Nilai harus berupa angka',
            'status.required' => 'Mohon pilih Status',
        ];

        $this->validate($req, [
            'nilai' => 'required|numeric|min:0|max:100',
            'status' => 'required',
            'pewawancara' => 'required',
        ], $messages);


        $current_partner = $vacancy_detail->current_partner + 1;

        if ($vacancy_detail->max_partner < $current_partner) {
            return redirect()->back()->with([
                'error' => 'Sorry, quota position of ' . $position->name . ' for this vacancy is full'
            ]);
        }

        $vacancy_detail->update([
            'current_partner' => $current_partner
        ]);


        if ($req->status == "Tidak Lulus") {
            $status_interview = "REJECTED";
        } else {
            $status_interview = "APPROVED";
        }

        $data->update([
            'status' => $status_interview
        ]);

        $interview->update([
            'nilai' => $req->nilai,
            'pewawancara' => $req->pewawancara,
            'keterangan' => $req->keterangan
        ]);



        if ($status_interview == "APPROVED") {
            $pesan_notif = 'Selamat anda Lolos!';
            $pesan_email = '<p>Selamat anda Lulus Lowongan <strong>' . $vacancy->title . '</strong></p>
       ';
            $status = 'lulus';
            $nilai = $req->nilai . '/100';
        } else {
            $pesan_notif = 'Maaf anda tidak lolos';
            $pesan_email = '<p>Mohon maaf, anda tidak Lulus Lowongan <strong>' . $vacancy->title . '</strong></p>
        ';
            $status = 'tidak lulus';
            $nilai = '-';
        }

        //simpan notif ke db
        Notification::create([
            'partner_id' => $data->partner_id,
            'pesan' =>  $pesan_notif,
        ]);

        //kirim live notif
        event(new StatusLiked($pesan_notif, $user->id));

        $data_partner = [
            'id' => $user->id,
            'nama' => $user->name,
            'tgl' => $user_detail->birth_place . ', ' . date('d F Y', strtotime($user_detail->birth_date)),
            'alamat' => $user_detail->address,
            'hp' => $user_detail->phone_number,
            'status' => $status,
            'lowongan' => $vacancy->title,
            'posisi' => $position->name,
            'nilai' => $nilai,
            'gambar' => $user->photo

        ];

        //Kirim email notifikasi
        //  try{
        //     Mail::send('email_interview', $data_partner, function ($message) use ($penirima)
        //     {
        //         $message->subject('Informasi Penerimaan Lowongan - Status Interview');
        //         $message->from('info@riskyarya.com', 'BPS Bangli');
        //         $message->to($penirima);
        //     });
        // }
        // catch (Exception $e){
        //     return response (['status' => true,'errors' => $e->getMessage()]);
        // }



        //INI SALAH
        //  try{
        //     Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan_email], function ($message) use ($penirima)
        //     {
        //         $message->subject('Informasi Penerimaan Lowongan - Status Administrasi');
        //         $message->from('info@riskyarya.com', 'BPS Bangli');
        //         $message->to($penirima);
        //     });

        //     }
        // catch (Exception $e){
        //     return response (['status' => true,'errors' => $e->getMessage()]);
        // }

        return redirect('admin/vacancy/' . $data->vacancy_id . '/applies')->with([
            'success' => 'Data successfully updated'
        ]);

        // return var_dump($data_partner);


    }
}
