<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Administrator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Administrator::orderBy('created_at','DESC')->get();

        return view('admin.administrator.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.administrator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:administrators,email',
            'password' => 'required|confirmed',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048'
        ]);

        $req = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/avatar/admin/', $file_name);
                $req['photo'] = url("uploads/avatar/admin/$file_name");
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            Administrator::create($req);
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Administrator::findOrFail($id);
        return view('admin.administrator.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Administrator::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:administrators,email,' . $id,
            'password' => 'sometimes|confirmed',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048'
        ]);

        $req = [
            'name' => $request->name,
            'email' => $request->email,
        ];

        if($request->password){
            $req['password'] =  Hash::make($request->password);
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/avatar/admin/', $file_name);
                $req['photo'] = url("uploads/avatar/admin/$file_name");
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            $data->update($req);
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Administrator::where('id', $id)->first();

        if(!$data){
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ]);
        }

        DB::beginTransaction();
        try{

            $data->delete();

            DB::commit();
        }
        catch(\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data successfully deleted'
        ]);
    }
}
