<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Announcement;
use Auth;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Announcement::orderBy('created_at','DESC')->get();

        return view('admin.announcement.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.announcement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'required|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);
        
        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];

        if($request->status){
            $req['status'] = "PUBLISHED";
        }
        else{
            $req['status'] = "UNPUBLISHED";
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/announcement/', $file_name);
                // $req['photo'] = url("uploads/announcement/$file_name");
                $req['photo'] = $file_name;
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            Announcement::create($req);
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Announcement::findOrFail($id);
        return view('admin.announcement.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Announcement::findOrFail($id);

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);
        
        $req = [
            'title' => $request->title,
            'description' => $request->description,
            'administrator_id' => Auth::guard('administrators')->user()->id
        ];

        if($request->status){
            $req['status'] = "PUBLISHED";
        }
        else{
            $req['status'] = "UNPUBLISHED";
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/announcement/', $file_name);
                // $req['photo'] = url("uploads/announcement/$file_name");
                $req['photo'] = $file_name;
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            $data->update($req);
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
                    ->back()
                    ->with('success', 'Data Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Announcement::where('id', $id)->first();

        if(!$data){
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ]);
        }

        DB::beginTransaction();
        try{

            $data->delete();

            DB::commit();
        }
        catch(\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data successfully deleted'
        ]);
    }
}
