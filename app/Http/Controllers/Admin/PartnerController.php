<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Partner;
use App\PartnerDetail;
use Artisan;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Partner::with(['detail'])->orderBy('created_at', 'DESC')->get();

        return view('admin.partner.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:partners,email',
            'password' => 'required|confirmed',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);

        $req = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];

        if ($request->status) {
            $req['status'] = "ACTIVE";
        } else {
            $req['status'] = "INACTIVE";
        }

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/avatar/partner/', $file_name);
                // $req['photo'] = url("uploads/avatar/partner/$file_name");
                $req['photo'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try {
            $data = Partner::create($req);
            PartnerDetail::create([
                'partner_id' => $data->id
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Partner::with(['detail'])->where('id', $id)->firstOrFail();
        return view('admin.partner.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Partner::findOrFail($id);
        return view('admin.partner.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Partner::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:partners,email,' . $id,
            'password' => 'sometimes|confirmed',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
            'status' => 'nullable|boolean'
        ]);

        $req = [
            'name' => $request->name,
            'email' => $request->email,
        ];


        if ($request->status) {
            $req['status'] = "ACTIVE";
        } else {
            $req['status'] = "INACTIVE";
        }

        if ($request->password) {
            $req['password'] =  Hash::make($request->password);
        }

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/avatar/partner/', $file_name);
                // $req['photo'] = url("uploads/avatar/partner/$file_name");
                $req['photo'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try {
            $data->update($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Data Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Partner::where('id', $id)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ]);
        }

        DB::beginTransaction();
        try {

            $data->delete();

            DB::commit();
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data successfully deleted'
        ]);
    }

    //CRONJOB CEK STATUS USER
    public function check_transaksi()
    {
        Artisan::call('crontupdatestatus_user:log');
        return Artisan::output();
    }
}
