<?php

namespace App\Http\Controllers;

use App\AboutUs;
use Illuminate\Http\Request;
use App\Announcement;
use App\Vacancy;
use App\Partner;
use App\Activity;
use App\Setting;
use Illuminate\Support\Facades\DB;
use App\PartnerDetail;
use App\VacancyDetail;
use Auth;
use App\VacancyApply;
use App\Position;
use App\Notification;
use App\Interview;
use App\Events\NotifAdmin;
use Carbon\Carbon;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */




    public function index()
    {

        $count_partner = Partner::where('status', 'ACTIVE')->count();

        $count_vacancy = Vacancy::where('status', 'ACTIVE')->count();

        $vacancies = Vacancy::with(['detail', 'detail.position'])->where('status', 'ACTIVE')->orderBy('created_at', 'DESC')
            ->limit(3)
            ->get();

        $announcements = Announcement::with(['administrator'])->where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->limit(3)
            ->get();

        $activities = Activity::where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->limit(3)
            ->get();

        $about = Setting::where('type', 'ABOUT_PAGE')->first();

        return view('home', [
            'about' => $about,
            'announcements' => $announcements,
            'vacancies' => $vacancies,
            'activities' => $activities,
            'count_partner' => $count_partner,
            'count_vacancy' => $count_vacancy
        ]);
    }

    public function about()
    {
        $about = Setting::where('type', 'ABOUT_PAGE')->first();

        $about_us = AboutUs::where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('about', [
            'about' => $about,
            'about_us' => $about_us,
        ]);
    }

    public function vacancy()
    {
        $vacancies = Vacancy::where('status', 'ACTIVE')->orderBy('created_at', 'DESC')
            ->get();

        return view('vacancy', [
            'vacancies' => $vacancies,
        ]);
    }

    public function vacancy_detail(Request $request, $id)
    {
        $data = Vacancy::with('detail.position')->where('id', $id)->where('status', 'ACTIVE')
            ->firstOrFail();

        if (Auth::guard('partners')->check()) {
            $vacancy_apply = VacancyApply::where('vacancy_id', $id)->where('partner_id', Auth::guard('partners')->user()->id)->orderBy('updated_at', 'DESC')->first();
        } else {
            $vacancy_apply = VacancyApply::where('vacancy_id', $id)->where('partner_id', "")->orderBy('updated_at', 'DESC')->first();
        }


        return view('vacancy-detail', [
            'data' => $data,
            'vacancy_apply' => $vacancy_apply
        ]);
    }

    public function vacancy_apply(Request $request, $id)
    {
        $detail = VacancyDetail::where('vacancy_id', $id)->where('position_id', $request->position_id)->firstOrFail();

        if ($detail->current_partner >= $detail->max_partner) {
            return redirect()->back()->with([
                'error' => 'Maaf, kuota posisi tersebut sudah penuh.'
            ]);
        }

        $req = [
            'position_id' => $request->position_id,
            'vacancy_id' => $id,
            'administrator_id' => null,
            'partner_id' => Auth::guard('partners')->user()->id,
            'status' => 'UNAPPROVED'
        ];

        DB::beginTransaction();
        try {
            $data = VacancyApply::create($req);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        $vacancy = Vacancy::where('id', $id)->firstOrFail();
        $position = Position::where('id', $request->position_id)->firstOrFail();
        $partner = Partner::where('id', Auth::guard('partners')->user()->id)->firstOrFail();

        $pesan_notif = 'Melamar Lowongan ' . $vacancy->title . ' Posisi ' . $position->name;

        Notification::create([
            'partner_id' => Auth::guard('partners')->user()->id,
            'pesan' =>  $pesan_notif,
        ]);

        //kirim live notif
        event(new NotifAdmin($pesan_notif, $partner->name,  $partner->photo, Carbon::now()->format('Y-m-d h:m:s')));

        // event(new App\Events\NotifAdmin('Lolosneee','Didik Ariyana','1605365067.png','2020-12-02 19:34:16'));


        return redirect()
            ->back()
            ->with('success', 'Berhasil melamar lowongan. Silahkan tunggu verifikasi dari petugas.');
    }

    public function announcement()
    {
        $announcements = Announcement::with(['administrator'])->where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('announcement', [
            'announcements' => $announcements,
        ]);
    }

    public function announcement_detail(Request $request, $id)
    {
        $data = Announcement::with(['administrator'])
            ->where('id', $id)
            ->where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->firstOrFail();

        return view('announcement-detail', [
            'data' => $data,
        ]);
    }

    public function activity()
    {
        $activities = Activity::where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('activity', [
            'activities' => $activities,
        ]);
    }

    public function profile(Request $request)
    {
        $id = \Auth::guard('partners')->user()->id;
        $data = Partner::with('detail')->where('id', $id)->first();

        return view('profile')->with([
            'data' => $data
        ]);
    }

    public function profile_update(Request $request)
    {
        $partner = \Auth::guard('partners')->user();

        $request->validate([
            'nama_lengkap' => 'required',
            'alamat_email' => 'required|unique:partners,email,' . $partner->id,
            'alamat' => 'nullable',
            'tempat_lahir' => 'nullable',
            'tanggal_lahir' => 'nullable|date',
            'nomor_telepon' => 'nullable|numeric',
            'tipe_telepon' => 'nullable',
            'pendidikan_terakhir' => 'nullable',
            'aktifitas_terakhir' => 'nullable',
            'ktp' => 'nullable|file',
            'ijazah' => 'nullable|file',
            'kk' => 'nullable|file',
        ]);

        $partner_payload = [
            'name' => $request->nama_lengkap,
            'email' => $request->alamat_email
        ];

        $partner_detail_payload = [
            'address' => $request->alamat,
            'birth_place' => $request->tempat_lahir,
            'birth_date' => $request->tanggal_lahir,
            'phone_number' => $request->nomor_telepon,
            'phone_type' => $request->tipe_telepon,
            'last_education' => $request->pendidikan_terakhir,
            'last_activity' => $request->aktifitas_terakhir,
        ];

        if ($request->status) {
            $partner_detail_payload['is_marriage'] = 1;
        } else {
            $partner_detail_payload['is_marriage'] = 0;
        }

        if ($request->hasFile('ktp')) {
            $file = $request->file('ktp');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/partner/files/', $file_name);
                $partner_detail_payload['ktp'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        if ($request->hasFile('ijazah')) {
            $file = $request->file('ijazah');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/partner/files/', $file_name);
                $partner_detail_payload['ijazah'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        if ($request->hasFile('kartu_keluarga')) {
            $file = $request->file('kartu_keluarga');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;

            try {
                $file->move('uploads/partner/files/', $file_name);
                $partner_detail_payload['kk'] = $file_name;
            } catch (\Exception $e) {
                return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try {
            Partner::where('id', $partner->id)->update($partner_payload);
            PartnerDetail::where('partner_id', $partner->id)->update($partner_detail_payload);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()
                ->back()
                ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }

        return redirect()
            ->back()
            ->with('success', 'Profil anda berhasil di update!');
    }

    public function applies(Request $request)
    {
        $data = VacancyApply::with([
            'position',
            'vacancy'
        ])->where('partner_id', Auth::guard('partners')->user()->id)->orderBy('created_at', "DESC")->get();

        return view('applies')->with([
            'data' => $data
        ]);
    }

    public function applies_detail($id)
    {
        $data = VacancyApply::with(['partner'])->where('id', $id)->firstOrFail();
        $user = Partner::where('id', $data->partner_id)->firstOrFail();
        $user_detail = PartnerDetail::where('partner_id', $data->partner_id)->firstOrFail();
        $interview = Interview::where('vacancy_applies_id', $id)->firstOrFail();
        $position = Position::where('id', $data->position_id)->firstOrFail();
        $vacancy = Vacancy::where('id', $data->vacancy_id)->firstOrFail();

        if ($data->status == "APPROVED") {
            $status = 'lulus';
            $nilai = $interview->nilai . '/100';
        } else {
            $status = 'tidak lulus';
            $nilai = '-';
        }

        $data_partner = [
            'id' => $user->id,
            'nama' => $user->name,
            'tgl' => $user_detail->birth_place . ', ' . date('d F Y', strtotime($user_detail->birth_date)),
            'alamat' => $user_detail->address,
            'hp' => $user_detail->phone_number,
            'status' => $status,
            'lowongan' => $vacancy->title,
            'posisi' => $position->name,
            'nilai' => $nilai,
            'keterangan' => $interview->keterangan,
            'gambar' => $user->photo,
        ];

        return view(
            'detail_applies',
            $data_partner
        );
    }

    public function applies_cancel(Request $request, $id)
    {
        $data = VacancyApply::where('id', $id)->firstOrFail();

        $vacancy_detail = VacancyDetail::where('vacancy_id', $data->vacancy_id)->where('position_id', $data->position_id)->firstOrFail();

        $position = Position::where('id', $vacancy_detail->position_id)->firstOrFail();

        $current_partner = $vacancy_detail->current_partner - 1;

        if ($current_partner <= 0) {
            $current_partner = 0;
        }

        if ($data->status != "UNAPPROVED") {
            $vacancy_detail->update([
                'current_partner' => $current_partner
            ]);
        }

        $data->delete();

        return redirect()->back()->with([
            'success' => 'Lamaran anda berhasil dibatalkan!'
        ]);
    }
}
