<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Partner;
use App\PartnerDetail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register_partner(Request $request){
        if(\Auth::guard('partners')->check()){
            return redirect()->intended('/account/profile');
        }
        return view('register');
    }

    public function register_partner_store(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:partners,email',
            'password' => 'required|confirmed',
            'photo' => 'nullable|file|mimes:jpeg,jpg,png|max:2048',
        ]);

        $req = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];

        $req['status'] = "ACTIVE";

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = time() . "." . $file_extension;
            
            try{
                $file->move('uploads/avatar/partner/', $file_name);
                $req['photo'] = $file_name;
            }
            catch(\Exception $e){
                return redirect()
                        ->back()
                        ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
            }
        }

        DB::beginTransaction();
        try{
            $data = Partner::create($req);
            PartnerDetail::create([
                'partner_id' => $data->id
            ]);
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()
                    ->back()
                    ->with('error', "Error on line {$e->getLine()} : {$e->getMessage()}");
        }
        
        if(\Auth::guard('partners')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ])){
            return redirect()->intended('/account/profile');
        }

    }
}
