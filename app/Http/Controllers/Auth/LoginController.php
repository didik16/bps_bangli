<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Administrator;
use Auth;
use App\Partner;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login_partner(Request $request){
        if(\Auth::guard('partners')->check()){
            return redirect()->intended('/account/profile');
        }
        return view('login');
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = Administrator::where('email', $request->email)->first();

            if(is_null($user)){
                $user = Partner::where('email', $request->email)->first();
                if($user){
                    if(Auth::guard('partners')->attempt([
                        'email' => $request->email,
                        'password' => $request->password
                    ])){
                        return redirect()->intended('/');
                    }else{
                        return redirect()
                            ->back()
                            ->with('error', 'This credential is not match in our record');
                    }
                }
                else{

                    return redirect()
                        ->back()
                        ->with('error', 'This credential is not match in our record');
                }
            } else{
                if(Auth::guard('administrators')->attempt([
                    'email' => $request->email,
                    'password' => $request->password
                ])){
                    return redirect()->intended('/admin/dashboard');
                }else{
                    return redirect()
                        ->back()
                        ->with('error', 'This credential is not match in our record');
                }
            }
    }

    public function logout(){
        Auth::guard('administrators')->logout();
        return redirect()->route('admin.login');
    }

    public function logout_partner(){
        Auth::guard('partners')->logout();
        return redirect()->route('login.index');
    }
}
