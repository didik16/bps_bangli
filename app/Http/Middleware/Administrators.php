<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Administrators
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('administrators')->check()){
            return $next($request);
        }
        return redirect()->route('admin.login');
    }
}
