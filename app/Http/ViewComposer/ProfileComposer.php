<?php

namespace App\Http\ViewComposer;

use Illuminate\View\View;
use App\Notification;
use Auth;

class ProfileComposer
{

    public function compose(View $view)
    {

        if (Auth::guard('partners')->check()) {
            $user_id = \Auth::guard('partners')->user()->id;
        } else {
            $user_id = "";
        }

        $notif = Notification::where('partner_id', $user_id)->Where('pesan', 'not like', '%' . 'melamar' . '%')->orderBy('created_at', 'DESC')
            ->limit(5)
            ->get();

        $view->with('components.header')->with(['notif' => $notif]);
    }
}
