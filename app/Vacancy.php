<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Vacancy extends Model
{
    protected $table = 'vacancies';

    protected $fillable = [
        'administrator_id',
        'photo',
        'title',
        'description',
        'max_partner',
        'end_period',
        'status'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }

    public function detail()
    {
        return $this->hasMany('App\VacancyDetail');
    }

    public function applies()
    {
        return $this->hasMany('App\VacancyApply');
    }

    public function interview()
    {
        return $this->hasMany('App\Interview');
    }
}
