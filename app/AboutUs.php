<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class AboutUs extends Model
{
    protected $table = 'about_us';

    protected $fillable = [
        'administrator_id',
        'photo',
        'title',
        'description',
        'status'
    ];

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        // Set UUID on boot.
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(1);
        });
    }
}
